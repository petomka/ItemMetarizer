package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Benedikt on 28.08.2017.
 */
public class ItemFlagGUI implements Listener {

    private Player player;
    private Inventory inventory;
    private Resumable resumable;
    private ItemStack toEdit;

    public ItemFlagGUI(Player player, Resumable resumable, ItemStack toEdit) {
        this.player = player;
        this.resumable = resumable;
        this.toEdit = toEdit;
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void dispose() {
        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
            resumable.resume();
            return null;
        });
    }

    private void createInventory() {
        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("flags-gui.title"));

        ItemStack blackPane = MainGUI.getBlackGlassPane();

        for(int i = 0; i < 54; i++) {
            inventory.setItem(i, blackPane);
        }

        ItemStack hidden = new ItemStack(Material.INK_SACK);
        ItemMeta hiddenM = hidden.getItemMeta();
        hiddenM.setDisplayName(ConfigManager.getString("flags-gui.hidden"));
        hidden.setDurability((short) 8);
        hidden.setItemMeta(hiddenM);

        ItemStack shown = new ItemStack(Material.INK_SACK);
        ItemMeta shownM = shown.getItemMeta();
        shownM.setDisplayName(ConfigManager.getString("flags-gui.shown"));
        shown.setDurability((short) 10);
        shown.setItemMeta(shownM);

        ItemStack attributes = new ItemStack(Material.COMMAND);
        ItemMeta attM = attributes.getItemMeta();
        attM.setDisplayName(ConfigManager.getString("flags-gui.hide-attributes"));
        attributes.setItemMeta(attM);
        inventory.setItem(10, attributes);
        if(toEdit.getItemMeta().hasItemFlag(ItemFlag.HIDE_ATTRIBUTES)) {
            inventory.setItem(19, hidden);
        } else {
            inventory.setItem(19, shown);
        }

        ItemStack enchants = new ItemStack(Material.ENCHANTMENT_TABLE);
        ItemMeta enchM = enchants.getItemMeta();
        enchM.setDisplayName(ConfigManager.getString("flags-gui.hide-enchants"));
        enchants.setItemMeta(enchM);
        inventory.setItem(11, enchants);
        if(toEdit.getItemMeta().hasItemFlag(ItemFlag.HIDE_ENCHANTS)) {
            inventory.setItem(20, hidden);
        } else {
            inventory.setItem(20, shown);
        }

        ItemStack potions = new ItemStack(Material.POTION);
        ItemMeta potM = potions.getItemMeta();
        potM.setDisplayName(ConfigManager.getString("flags-gui.hide-potions"));
        potions.setItemMeta(potM);
        inventory.setItem(12, potions);
        if(toEdit.getItemMeta().hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS)) {
            inventory.setItem(21, hidden);
        } else {
            inventory.setItem(21, shown);
        }

        ItemStack destroy = new ItemStack(Material.IRON_PICKAXE);
        ItemMeta desM = destroy.getItemMeta();
        desM.setDisplayName(ConfigManager.getString("flags-gui.hide-destroys"));
        destroy.setItemMeta(desM);
        inventory.setItem(14, destroy);
        if(toEdit.getItemMeta().hasItemFlag(ItemFlag.HIDE_DESTROYS)) {
            inventory.setItem(23, hidden);
        } else {
            inventory.setItem(23, shown);
        }

        ItemStack place = new ItemStack(Material.LOG);
        ItemMeta plM = place.getItemMeta();
        plM.setDisplayName(ConfigManager.getString("flags-gui.hide-placed-on"));
        place.setItemMeta(plM);
        inventory.setItem(15, place);
        if(toEdit.getItemMeta().hasItemFlag(ItemFlag.HIDE_PLACED_ON)) {
            inventory.setItem(24, hidden);
        } else {
            inventory.setItem(24, shown);
        }

        ItemStack unbreakable = new ItemStack(Material.BEDROCK);
        ItemMeta unbM = unbreakable.getItemMeta();
        unbM.setDisplayName(ConfigManager.getString("flags-gui.hide-unbreakable"));
        unbreakable.setItemMeta(unbM);
        inventory.setItem(16, unbreakable);
        if(toEdit.getItemMeta().hasItemFlag(ItemFlag.HIDE_UNBREAKABLE)) {
            inventory.setItem(25, hidden);
        } else {
            inventory.setItem(25, shown);
        }

        ItemStack exit = new ItemStack(Material.IRON_DOOR);
        ItemMeta exitM = exit.getItemMeta();
        exitM.setDisplayName(ConfigManager.getString("flags-gui.exit-to-menu"));
        exit.setItemMeta(exitM);
        inventory.setItem(45, exit);

        ItemStack bluePane = MainGUI.getBlueGlassPane();
        for(int i = 46; i < 54; i++) {
            inventory.setItem(i, bluePane);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if(event.getRawSlot() == 45) {
            dispose();
        } else if(event.getRawSlot() == 19) {
            toggleFlag(ItemFlag.HIDE_ATTRIBUTES, 19);
        } else if(event.getRawSlot() == 20) {
            toggleFlag(ItemFlag.HIDE_ENCHANTS, 20);
        } else if(event.getRawSlot() == 21) {
            toggleFlag(ItemFlag.HIDE_POTION_EFFECTS, 21);
        } else if(event.getRawSlot() == 23) {
            toggleFlag(ItemFlag.HIDE_DESTROYS, 23);
        } else if(event.getRawSlot() == 24) {
            toggleFlag(ItemFlag.HIDE_PLACED_ON, 24);
        } else if(event.getRawSlot() == 25) {
            toggleFlag(ItemFlag.HIDE_UNBREAKABLE, 25);
        }
    }

    private void toggleFlag(ItemFlag flag, int slotToUpdate) {
        ItemMeta meta = toEdit.getItemMeta();
        if(meta.hasItemFlag(flag)) {
            meta.removeItemFlags(flag);
            ItemStack shown = new ItemStack(Material.INK_SACK);
            ItemMeta shownM = shown.getItemMeta();
            shownM.setDisplayName(ConfigManager.getString("flags-gui.shown"));
            shown.setDurability((short) 10);
            shown.setItemMeta(shownM);
            inventory.setItem(slotToUpdate, shown);
        } else {
            meta.addItemFlags(flag);
            ItemStack hidden = new ItemStack(Material.INK_SACK);
            ItemMeta hiddenM = hidden.getItemMeta();
            hiddenM.setDisplayName(ConfigManager.getString("flags-gui.hidden"));
            hidden.setDurability((short) 8);
            hidden.setItemMeta(hiddenM);
            inventory.setItem(slotToUpdate, hidden);
        }
        toEdit.setItemMeta(meta);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        dispose();
    }

}
