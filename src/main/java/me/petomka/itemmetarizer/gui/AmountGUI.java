package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;

public class AmountGUI implements Listener {

    private Player player;
    private ItemStack toEdit;
    private Resumable resumable;

    private Inventory inventory;

    private int oversizedStacks = Main.main.getConfig().getBoolean("allow-oversized-stacks") ? 63 : 0;

    public AmountGUI(Player player, ItemStack toEdit, Resumable resumable) {
        this.player = player;
        this.toEdit = toEdit;
        this.resumable = resumable;
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void createInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("amount-gui.title"));
        }

        ItemStack blackPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte)15);
        ItemMeta blackMeta = blackPane.getItemMeta();
        blackMeta.setDisplayName("§r");
        blackPane.setItemMeta(blackMeta);

        for(int i = 0; i < 45; i++) {
            inventory.setItem(i, blackPane);
        }

        blackPane.setDurability((short)11);

        for(int i = 45; i < 54; i++) {
            inventory.setItem(i, blackPane);
        }

        for (int a = 0; a < 2; a++) {
            for (int b = 0; b < 5; b++) {
                int amount = (b + 1) * Math.max(1, (a > 0 ? 10 : 0));
                ItemStack amountMod = new ItemStack(Material.MELON_SEEDS, amount);
                ItemMeta amountMeta = amountMod.getItemMeta();
                amountMeta.setDisplayName(ConfigManager.getString("amount-gui.add").replace("<amount>", String.valueOf(amount)));
                amountMeta.setLore(Collections.singletonList(ConfigManager.getString("amount-gui.subtract").replace("<amount>", String.valueOf(amount))));
                amountMod.setItemMeta(amountMeta);

                inventory.setItem((a + 2) * 9 + b + 2, amountMod);
            }
        }

        ItemStack back = new ItemStack(Material.IRON_DOOR);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName(ConfigManager.getString("amount-gui.back"));
        back.setItemMeta(backMeta);

        inventory.setItem(45, back);

        inventory.setItem(13, toEdit);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if(event.getRawSlot() == 45) {
            dispose();
            return;
        }
        int row = event.getRawSlot()/9;
        int column = event.getRawSlot()%9;
        if(row < 2 || column < 2 || row > 3 || column > 6) {
            return;
        }
        int amount = toEdit.getAmount() + (event.isLeftClick() ? 1 : (event.isRightClick() ? -1 : 0)) * (column-2+1) * Math.max(1, (row-2 > 0 ? 10 : 0)) ;
        if(amount < 1) {
            amount = 1;
        } else if(amount > 64 + oversizedStacks) {
            amount = 64 + oversizedStacks;
        }
        toEdit.setAmount(amount);
        player.getInventory().setItemInMainHand(toEdit);
        createInventory();
    }

    private void dispose() {
        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
            resumable.resume();
            return null;
        });
    }

}
