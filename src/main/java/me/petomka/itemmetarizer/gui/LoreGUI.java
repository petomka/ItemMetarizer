package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Benedikt on 25.08.2017.
 */
public class LoreGUI implements Listener, Resumable {

    private Player player;

    private ItemStack itemStack;

    private Inventory inventory;

    private Resumable resumable;

    private List<String> loreList;

    private int lastIndexToEditLore = -1;

    public LoreGUI(Player player, ItemStack itemStack, Resumable resumable) {
        this.player = player;
        this.itemStack = itemStack;
        this.resumable = resumable;
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void createInventory() {
        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("lore-gui.title"));

        fillInventoryWithBlackPanes();

        loreList = itemStack.getItemMeta().getLore();
        if (loreList == null) {
            loreList = new ArrayList<>();
        }

        fillInventoryWithPaper();

        ItemStack bluePane = new ItemStack(Material.STAINED_GLASS_PANE);
        ItemMeta blueMeta = bluePane.getItemMeta();
        blueMeta.setDisplayName("§r");
        bluePane.setDurability((short) 11);
        bluePane.setItemMeta(blueMeta);


        for (int i = 46; i < 53; i++) {
            inventory.setItem(i, bluePane);
        }

        ItemStack backToMenu = new ItemStack(Material.IRON_DOOR);
        ItemMeta backMeta = backToMenu.getItemMeta();
        backMeta.setDisplayName(ConfigManager.getString("lore-gui.back-to-menu"));
        backToMenu.setItemMeta(backMeta);
        inventory.setItem(45, backToMenu);

        ItemStack deleteAll = new ItemStack(Material.BARRIER);
        ItemMeta deleteMeta = deleteAll.getItemMeta();
        deleteMeta.setDisplayName(ConfigManager.getString("lore-gui.delete-all"));
        deleteAll.setItemMeta(deleteMeta);
        inventory.setItem(53, deleteAll);
    }

    private void fillInventoryWithBlackPanes() {
        ItemStack blackPane = MainGUI.getBlackGlassPane();

        for (int i = 0; i < 45; i++) {
            inventory.setItem(i, blackPane);
        }
    }

    private void fillInventoryWithPaper() {
        String linePattern = ConfigManager.getString("lore-gui.line-pattern");
        String moveLine = ConfigManager.getString("lore-gui.move-line");
        String editLine = ConfigManager.getString("lore-gui.edit-line");
        String deleteLine = ConfigManager.getString("lore-gui.delete-line");

        for (int i = 0; i < loreList.size() + 2 && i < 45; i++) {
            if (i == loreList.size()) {
                setGreenAddPane(i);
                continue;
            } else if (i == loreList.size() + 1) {
                inventory.setItem(i, MainGUI.getBlackGlassPane());
                break;
            }
            ItemStack paper = new ItemStack(Material.PAPER);
            ItemMeta meta = paper.getItemMeta();
            meta.setDisplayName(linePattern.replace("<line>", String.valueOf(i + 1)));
            meta.setLore(Arrays.asList(loreList.get(i), moveLine, editLine, deleteLine));
            paper.setItemMeta(meta);
            inventory.setItem(i, paper);
        }
        player.updateInventory();
    }

    private void setGreenAddPane(int index) {
        ItemStack add = new ItemStack(Material.STAINED_GLASS_PANE);
        ItemMeta addMeta = add.getItemMeta();
        addMeta.setDisplayName(ConfigManager.getString("lore-gui.add-line"));
        add.setDurability((short) 13);
        add.setItemMeta(addMeta);
        inventory.setItem(index, add);
    }

    private void dispose(boolean resume) {
        HandlerList.unregisterAll(this);
        if (resume) {
            Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
                resumable.resume(MainGUI.Property.lore);
                return null;
            });
        }
    }

    private boolean hasDragAndDropStarted = false;

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if (event.getRawSlot() == 45) {
            dispose(true);
            return;
        }
        if (event.getRawSlot() == loreList.size()) {
            //add line
            if(hasDragAndDropStarted) {
                return;
            }
            lastIndexToEditLore = loreList.size();
            dispose(false);
            new ChatConfigurator(player, MainGUI.Property.lore, this, ChatConfigurator.VALUE_TYPE.STRING);
        } else if (event.getRawSlot() == 53) {
            //delete all
            if(hasDragAndDropStarted) {
                return;
            }
            loreList.clear();
            ItemMeta meta = itemStack.getItemMeta();
            meta.setLore(loreList);
            itemStack.setItemMeta(meta);
            fillInventoryWithBlackPanes();
            fillInventoryWithPaper();
        } else {
            //edit line / move line / delete line
            switch (event.getClick()) {
                case MIDDLE: {
                    if (!(event.getRawSlot() < loreList.size())) {
                        return;
                    }
                    dispose(false);
                    lastIndexToEditLore = event.getRawSlot();
                    new ChatConfigurator(player, MainGUI.Property.lore, this, ChatConfigurator.VALUE_TYPE.STRING);
                    break;
                }
                case LEFT: {
                    if(event.getSlot() >= loreList.size()) {
                        return;
                    }
                    if(hasDragAndDropStarted) {
                        event.setCancelled(false);
                        if(event.getCurrentItem() != null && event.getAction() == InventoryAction.PLACE_ALL) {
                            hasDragAndDropStarted = false;
                            Bukkit.getScheduler().runTaskLater(Main.main, this::parseNewLore, 1L);
                        }
                    } else {
                        hasDragAndDropStarted = true;
                        event.setCancelled(false);
                    }
                    break;
                }
                case RIGHT: {
                    if (!(event.getRawSlot() < loreList.size())) {
                        return;
                    }
                    deleteLine(event.getRawSlot());
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        dispose(true);
    }

    @Override
    public void resume(Object... params) {
        if (params.length == 0) {
            throw new IllegalArgumentException("Illegal call on resume() in LoreGUI");
        }
        MainGUI.Property property = MainGUI.Property.valueOf(String.valueOf(params[0]));
        if(property == MainGUI.Property.lore) {
            setLoreLine(String.valueOf(params[1]));
        } else {
            return;
        }
//        if (!(params[0] instanceof MainGUI.Property)) {
//            throw new IllegalArgumentException("Illegal call on resume() in LoreGUI");
//        }
//        MainGUI.Property prop = (MainGUI.Property) params[0];
//        switch (prop) {
//            case lore: {
//                setLoreLine((String) params[1]);
//                break;
//            }
//        }
        updateItemStack();
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    public void updateItemStack() {
        itemStack = player.getInventory().getItemInMainHand();
    }

    private void setLoreLine(String line) {
        if(lastIndexToEditLore >= loreList.size()) {
            lastIndexToEditLore = loreList.size();
            loreList.add(line);
            ItemMeta meta = itemStack.getItemMeta();
            meta.setLore(loreList);
            itemStack.setItemMeta(meta);
            fillInventoryWithPaper();
            return;
        }
        ItemStack stack = inventory.getItem(lastIndexToEditLore);
        ItemMeta stackMeta = stack.getItemMeta();
        List<String> lore = stackMeta.getLore();
        lore.set(0, line);
        stackMeta.setLore(lore);
        stack.setItemMeta(stackMeta);
        inventory.setItem(lastIndexToEditLore, stack);
        loreList.set(lastIndexToEditLore, line);
        ItemMeta toEditMeta = itemStack.getItemMeta();
        List<String> toEditLore = toEditMeta.getLore();
        toEditLore.set(lastIndexToEditLore, line);
        toEditMeta.setLore(toEditLore);
        itemStack.setItemMeta(toEditMeta);
    }

    private void deleteLine(int line) {
        if(line < 0 || line >= loreList.size()) {
            return;
        }
        loreList.remove(line);

        ItemMeta meta = itemStack.getItemMeta();
        meta.setLore(loreList);
        itemStack.setItemMeta(meta);

        fillInventoryWithPaper();
    }

    private void parseNewLore() {
        ArrayList<String> newLoreToSet = new ArrayList<>();
        for (int i = 0; i < 45; i++) {
            ItemStack itemStack = inventory.getItem(i);
            if (itemStack == null || itemStack.getType() != Material.PAPER || !itemStack.hasItemMeta()) {
                continue;
            }
            List<String> lore = itemStack.getItemMeta().getLore();
            if (lore == null || lore.isEmpty()) {
                continue;
            }
            newLoreToSet.add(lore.get(0));
        }
        this.loreList = newLoreToSet;
        ItemMeta meta = itemStack.getItemMeta();
        meta.setLore(newLoreToSet);
        itemStack.setItemMeta(meta);
        fillInventoryWithPaper();
    }
}
