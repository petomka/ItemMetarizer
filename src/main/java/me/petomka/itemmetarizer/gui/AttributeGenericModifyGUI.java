package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.attributeapi.Attribute;
import me.petomka.itemmetarizer.attributeapi.AttributeAPI;
import me.petomka.itemmetarizer.attributeapi.AttributeInfo;
import me.petomka.itemmetarizer.attributeapi.Operation;
import me.petomka.itemmetarizer.attributeapi.Slot;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by Benedikt on 29.08.2017.
 */
public class AttributeGenericModifyGUI implements Listener, Resumable {

    //i excuse for the mess i was tired af when creating this, had no internet due to a fire on some cables and refused to go to bed early.
    private Player player;
    private ItemStack toEdit, attributeIcon;
    private Resumable resumable;
    private Attribute attribute;
    private List<Material> materials;

    private boolean isAttribute = true;

    private AttributeAPI attributeAPI;

    private Inventory inventory;

    public AttributeGenericModifyGUI(Player player, ItemStack toEdit, Resumable resumable, Attribute attribute, AttributeAPI api, ItemStack attributeIcon) {
        this.player = player;
        this.toEdit = toEdit;
        this.attributeIcon = attributeIcon;
        this.resumable = resumable;
        this.attribute = attribute;
        this.attributeAPI = api;
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void createInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("attributes-gui.title"));
        }

        ItemStack blackPane = MainGUI.getBlackGlassPane();

        for (int i = 0; i < 45; i++) {
            inventory.setItem(i, blackPane);
        }

        ItemStack exit = new ItemStack(Material.IRON_DOOR);
        ItemMeta exitMeta = exit.getItemMeta();
        exitMeta.setDisplayName(ConfigManager.getString("attributes-gui.exit"));
        exit.setItemMeta(exitMeta);
        inventory.setItem(45, exit);

        ItemStack bluePane = MainGUI.getBlueGlassPane();

        for (int i = 46; i < 54; i++) {
            inventory.setItem(i, bluePane);
        }

        if (attribute == Attribute.CAN_PLACED_ON || attribute == Attribute.CAN_DESTROY) {
            isAttribute = false;
            createPlaceBreakInventory();
        } else {
            createAttributeInventory();
        }
    }

    private int placeBreakPage = 0;

    private void createPlaceBreakInventory() {
        AttributeAPI attributeAPI = new AttributeAPI(toEdit);
        inventory.clear();
        if(attribute == Attribute.CAN_DESTROY) {
            materials = attributeAPI.getCanDestroy();
        } else {
            materials = attributeAPI.getCanPlaceOn();
        }
        if(materials == null) {
            materials = new ArrayList<>();
        }
        for(int i = 0; i + 45*placeBreakPage < materials.size() && i < 45; i++) {
            inventory.setItem(i, new ItemStack(materials.get(i + placeBreakPage*45)));
        }

        ItemStack back = new ItemStack(Material.IRON_DOOR);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName(ConfigManager.getString("attributes-gui.break-place.back"));
        back.setItemMeta(backMeta);

        ItemStack nextp = new ItemStack(Material.GHAST_TEAR);
        ItemMeta nm = nextp.getItemMeta();
        nm.setDisplayName(ConfigManager.getString("attributes-gui.break-place.next-page"));
        nextp.setItemMeta(nm);

        ItemStack prevp = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta pm = prevp.getItemMeta();
        pm.setDisplayName(ConfigManager.getString("attributes-gui.break-place.prev-page"));
        prevp.setItemMeta(pm);

        ItemStack blackg = new ItemStack(Material.BLACK_STAINED_GLASS_PANE, 1);
        ItemMeta bm = blackg.getItemMeta();
        bm.setDisplayName("§r");
        blackg.setItemMeta(bm);

        for(int i = 46; i < 52; i++) {
            inventory.setItem(i, blackg);
        }

        inventory.setItem(45, back);
        inventory.setItem(52, prevp);
        inventory.setItem(53, nextp);

    }

    private void createAttributeInventory() {
        if (attributeAPI == null) {
            return;
        }

        AttributeInfo info = attributeAPI.getAttributeInfo(attribute);

        boolean ii = info != null;

        ItemStack addFlat = new ItemStack(Material.IRON_NUGGET);
        ItemMeta addFlatMeta = addFlat.getItemMeta();
        addFlatMeta.setDisplayName(ConfigManager.getString("attributes-gui.add-flat"));
        if (ii && info.operation == Operation.ADD_FLAT) {
            addFlatMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            addFlatMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        addFlat.setItemMeta(addFlatMeta);
        inventory.setItem(10, addFlat);

        ItemStack addPerc = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta addPercMeta = addPerc.getItemMeta();
        addPercMeta.setDisplayName(ConfigManager.getString("attributes-gui.add-percentage"));
        if (ii && info.operation == Operation.ADD_PERCENTAGE) {
            addPercMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            addPercMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        addPerc.setItemMeta(addPercMeta);
        inventory.setItem(19, addPerc);

        ItemStack multPerc = new ItemStack(Material.GOLD_BLOCK);
        ItemMeta multPercMeta = multPerc.getItemMeta();
        multPercMeta.setDisplayName(ConfigManager.getString("attributes-gui.multiply-percentage"));
        if (ii && info.operation == Operation.MULTIPLY_PERCENTAGE) {
            multPercMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            multPercMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        multPerc.setItemMeta(multPercMeta);
        inventory.setItem(28, multPerc);

        ItemStack mainHand = new ItemStack(Material.IRON_PICKAXE);
        ItemMeta mainHandMeta = mainHand.getItemMeta();
        mainHandMeta.setDisplayName(ConfigManager.getString("attributes-gui.slot-mainhand"));
        if (ii && info.slot == Slot.MAIN_HAND) {
            mainHandMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.active-slot")));
            mainHandMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            mainHandMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        } else {
            mainHandMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.left-click-set-slot")));
        }
        mainHand.setItemMeta(mainHandMeta);
        inventory.setItem(12, mainHand);

        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
        ItemMeta helmetMeta = helmet.getItemMeta();
        helmetMeta.setDisplayName(ConfigManager.getString("attributes-gui.slot-helmet"));
        if (ii && info.slot == Slot.HEAD) {
            helmetMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.active-slot")));
            helmetMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            helmetMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        } else {
            helmetMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.left-click-set-slot")));
        }
        helmet.setItemMeta(helmetMeta);
        inventory.setItem(13, helmet);

        ItemStack breast = new ItemStack(Material.LEATHER_CHESTPLATE);
        ItemMeta breastMeta = breast.getItemMeta();
        breastMeta.setDisplayName(ConfigManager.getString("attributes-gui.slot-breast"));
        if (ii && info.slot == Slot.CHEST) {
            breastMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.active-slot")));
            breastMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            breastMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        } else {
            breastMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.left-click-set-slot")));
        }
        breast.setItemMeta(breastMeta);
        inventory.setItem(14, breast);

        ItemStack legs = new ItemStack(Material.LEATHER_LEGGINGS);
        ItemMeta legMeta = legs.getItemMeta();
        legMeta.setDisplayName(ConfigManager.getString("attributes-gui.slot-legs"));
        if (ii && info.slot == Slot.LEGS) {
            legMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.active-slot")));
            legMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            legMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        } else {
            legMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.left-click-set-slot")));
        }
        legs.setItemMeta(legMeta);
        inventory.setItem(15, legs);

        ItemStack feet = new ItemStack(Material.LEATHER_BOOTS);
        ItemMeta feetMeta = feet.getItemMeta();
        feetMeta.setDisplayName(ConfigManager.getString("attributes-gui.slot-feet"));
        if (ii && info.slot == Slot.FEET) {
            feetMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.active-slot")));
            feetMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            feetMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        } else {
            feetMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.left-click-set-slot")));
        }
        feet.setItemMeta(feetMeta);
        inventory.setItem(16, feet);

        ItemStack offHand = new ItemStack(Material.TORCH);
        ItemMeta offMeta = offHand.getItemMeta();
        offMeta.setDisplayName(ConfigManager.getString("attributes-gui.slot-offhand"));
        if (ii && info.slot == Slot.OFF_HAND) {
            offMeta.setLore(Collections.singletonList(ConfigManager.getString("attributes-gui.active-slot")));
            offMeta.addEnchant(Enchantment.DURABILITY, 1, true);
            offMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        offHand.setItemMeta(offMeta);
        inventory.setItem(21, offHand);

        ItemStack setAmount = new ItemStack(Material.GLOWSTONE_DUST);
        ItemMeta amoMeta = setAmount.getItemMeta();
        amoMeta.setDisplayName(ConfigManager.getString("attributes-gui.set-amount"));
        setAmount.setItemMeta(amoMeta);
        inventory.setItem(33, setAmount);

        inventory.setItem(40, attributeIcon);

    }

    private void dispose(boolean resume) {
        HandlerList.unregisterAll(this);
        if (resume) {
            Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
                resumable.resume();
                return null;
            });
        }
    }

    public void resume(Object... params) {
        if (params.length != 2) {
            return;
        }
        updateItemStack();
        try {
            setAmount(Double.parseDouble(String.valueOf(params[1])));
            createInventory();
            player.openInventory(inventory);
            Bukkit.getPluginManager().registerEvents(this, Main.main);
        } catch (Exception e) {
            e.printStackTrace();
            new ChatConfigurator(player, MainGUI.Property.attributes, this, ChatConfigurator.VALUE_TYPE.STRING);
        }
    }

    public void updateItemStack() {
        toEdit = player.getInventory().getItemInMainHand();
    }

    private void updatePlayersItem() {
        player.getInventory().setItemInMainHand(toEdit);
    }

    private void setAmount(double d) {
        AttributeInfo info = attributeAPI.getAttributeInfo(attribute);
        if(info == null) {
            attributeAPI.addAttribute(attribute, Operation.ADD_FLAT, Slot.MAIN_HAND, d, attribute.getName(), UUID.randomUUID());
        } else {
            info.setAmount(d);
            attributeAPI.addAttribute(info);
        }
        attributeAPI.applyModifiers();
        player.getInventory().setItemInMainHand(attributeAPI.getItemStack());
        toEdit = attributeAPI.getItemStack();
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if (event.getRawSlot() == 45) {
            dispose(true);
        } else if (isAttribute) {
            attributeClick(event);
        } else {
            breakPlaceClick(event);
        }
    }

    private void attributeClick(InventoryClickEvent event) {
        AttributeInfo info = attributeAPI.getAttributeInfo(attribute);
        if (event.getRawSlot() == 33) {
            dispose(false);
            new ChatConfigurator(player, MainGUI.Property.attributes, this, ChatConfigurator.VALUE_TYPE.DOUBLE);
            return;
        }
        if (info == null) {
            player.sendMessage(ConfigManager.getString("attributes-gui.specify-amount-first"));
            return;
        }
        if (event.getRawSlot() == 10) {
            info.setOperation(Operation.ADD_FLAT);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        } else if (event.getRawSlot() == 19) {
            info.setOperation(Operation.ADD_PERCENTAGE);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        } else if (event.getRawSlot() == 28) {
            info.setOperation(Operation.MULTIPLY_PERCENTAGE);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        } else if(event.getRawSlot() == 12) {
            info.setSlot(Slot.MAIN_HAND);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        } else if(event.getRawSlot() == 13) {
            info.setSlot(Slot.HEAD);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        } else if(event.getRawSlot() == 14) {
            info.setSlot(Slot.CHEST);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        } else if(event.getRawSlot() == 15) {
            info.setSlot(Slot.LEGS);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        } else if(event.getRawSlot() == 16) {
            info.setSlot(Slot.FEET);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        } else if(event.getRawSlot() == 21) {
            info.setSlot(Slot.OFF_HAND);
            attributeAPI.addAttribute(info);
            attributeAPI.applyModifiers();
            toEdit = attributeAPI.getItemStack();
            createInventory();
            player.updateInventory();
            updatePlayersItem();
            updateItemStack();
        }
    }

    private void breakPlaceClick(InventoryClickEvent event) {
        if(event.getRawSlot() == 52) {
            if(placeBreakPage > 0) {
                placeBreakPage -= 1;
                createPlaceBreakInventory();
            }
            return;
        }
        if(event.getRawSlot() == 53) {
            int maxpage = materials.size() / 45;
            if(placeBreakPage < maxpage) {
                placeBreakPage += 1;
                createPlaceBreakInventory();
            }
            return;
        }
        if(event.getRawSlot() == 45) {
            dispose(true);
            return;
        }
        if(event.getRawSlot() < 45 && event.getRawSlot() + 45*placeBreakPage < materials.size()) {
            materials.remove(event.getRawSlot() + 45*placeBreakPage);
        }
        if(event.getRawSlot() >= 54 && event.getCurrentItem() != null && !materials.contains(event.getCurrentItem().getType())) {
            materials.add(event.getCurrentItem().getType());
        }
        if(attribute == Attribute.CAN_DESTROY) {
            attributeAPI.setCanDestroy(materials.toArray(new Material[0]));
        } else {
            attributeAPI.setCanPlaceOn(materials.toArray(new Material[0]));
        }
        toEdit = attributeAPI.getItemStack();
        createPlaceBreakInventory();
        player.updateInventory();
        updatePlayersItem();
        updateItemStack();
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        dispose(true);
    }
}
