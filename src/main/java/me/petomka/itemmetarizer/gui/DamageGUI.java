package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Benedikt on 28.08.2017.
 */
public class DamageGUI implements Listener, Resumable {

    public enum DamageTypeChange {
        stone(Material.STONE, 6),
        dirt(Material.DIRT, 2),
        oakwoodplank(Material.WOOD, 5),
        sapling(Material.SAPLING, 5),
        sand(Material.SAND, 1),
        woodlog(Material.LOG, 3),
        leaves(Material.LEAVES, 3),
        sponge(Material.SPONGE, 1),
        sandstone(Material.SANDSTONE, 2),
        grass(Material.LONG_GRASS, 1, 2),
        wool(Material.WOOL, 15),
        flower(Material.RED_ROSE, 8),
        slab(Material.STEP, 7),
        stainedglass(Material.STAINED_GLASS, 15),
        stonebrickmonsteregg(Material.MONSTER_EGGS, 5),
        stonebrick(Material.SMOOTH_BRICK, 3),
        woodslab(Material.WOOD_STEP, 5),
        cobblewall(Material.COBBLE_WALL, 1),
        anvil(Material.ANVIL, 2),
        quartzblock(Material.QUARTZ_BLOCK, 2),
        terracotta(Material.WHITE_GLAZED_TERRACOTTA, 15),
        stainedglasspane(Material.STAINED_GLASS_PANE, 15),
        leaves2(Material.LEAVES_2, 1),
        woodlog2(Material.LOG_2, 1),
        prismarine(Material.PRISMARINE, 2),
        carpet(Material.CARPET, 15),
        doubleplant(Material.DOUBLE_PLANT, 5),
        redsandstone(Material.RED_SANDSTONE, 2),
        stoneslab2(Material.STONE_SLAB2, 0),
        concrete(Material.CONCRETE, 15),
        concretepowder(Material.CONCRETE_POWDER, 15),
        goldenapple(Material.GOLDEN_APPLE, 1),
        fish(Material.RAW_FISH, 3),
        cookedfish(Material.COOKED_FISH, 1),
        inksack(Material.INK_SACK, 15),
        bed(Material.BED, 15),
        skull(Material.SKULL_ITEM, 5),
        banner(Material.BANNER, 15);
        Material material;
        int min;
        int max;
        DamageTypeChange(Material material, int min, int max) {
            this.material = material;
            this.max = max;
            this.min = min;
        }
        DamageTypeChange(Material material, int max) {
            this(material, 0, max);
        }
    }

    private Player player;
    private ItemStack toEdit;
    private Resumable resumable;
    private Inventory inventory;
    private DamageTypeChange changePattern = null;

    public DamageGUI(Player player, ItemStack toEdit, Resumable resumable) {
        this.player = player;
        this.toEdit = toEdit;
        this.resumable = resumable;
        createInventory();
    }

    private void dispose(boolean resume) {
        HandlerList.unregisterAll(this);
        if(resume) {
            Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
                resumable.resume();
                return null;
            });
        }
    }

    private void createInventory() {
        for(DamageTypeChange damageTypeChange : DamageTypeChange.values()) {
            if(toEdit.getType() == damageTypeChange.material) {
                changePattern = damageTypeChange;
                break;
            }
        }
        if(changePattern == null) {
            dispose(false);
            new DamageModifyGUI(player, resumable, toEdit, toEdit);
            return;
        }
        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("damage-gui.title"));
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
//        ItemStack blackPane = MainGUI.getBlackGlassPane();
//        for(int i = 0; i < 45; i++) {
//            inventory.setItem(i, blackPane);
//        }
        for(int i = 0; changePattern.min+i <= changePattern.max; i++) {
            inventory.setItem(i, new ItemStack(changePattern.material, 1, (short) (changePattern.min + i)));
        }
        ItemStack bluePane = MainGUI.getBlueGlassPane();
        for(int i = 46; i < 54; i++) {
            inventory.setItem(i, bluePane);
        }
        ItemStack exit = new ItemStack(Material.IRON_DOOR);
        ItemMeta exitMeta = exit.getItemMeta();
        exitMeta.setDisplayName(ConfigManager.getString("damage-gui.exit-to-menu"));
        exit.setItemMeta(exitMeta);
        inventory.setItem(45, exit);

        ItemStack editValue = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta editMeta = editValue.getItemMeta();
        editMeta.setDisplayName(ConfigManager.getString("damage-gui.edit-value"));
        editValue.setItemMeta(editMeta);
        inventory.setItem(53, editValue);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if(event.getRawSlot() == 45) {
            dispose(true);
        } else if(event.getRawSlot() == 53 && changePattern != null) {
            dispose(false);
            new DamageModifyGUI(player, this, toEdit, new ItemStack(changePattern.material));
        } else if(changePattern != null && (event.getRawSlot() >= 0 && event.getRawSlot() <= changePattern.max - changePattern.min)) {
            toEdit.setDurability((short) event.getRawSlot());
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        dispose(true);
    }

    public void resume(Object... params) {
        updateItemStack();
        createInventory();
    }

    public void updateItemStack() {
        toEdit = player.getInventory().getItemInMainHand();
    }

}
