package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class PotionEffectTypeGUI implements Listener, Resumable {

    private Player player;

    private Resumable resumable;

    private Inventory inventory;

    private PotionEffectType potionEffectType;
    private ItemStack toEdit;
    private PotionEffect potionEffect;

    public PotionEffectTypeGUI(Player player, Resumable resumable, PotionEffectType potionEffectType, ItemStack toEdit) {
        this.player = player;
        this.resumable = resumable;
        this.potionEffectType = potionEffectType;
        this.toEdit = toEdit;
        createInventory();
        player.openInventory(this.inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    int[] modifyRanges = new int[]{1, 2, 3};
    int[] timeRanges = new int[]{1, 5, 20, 60};

    private void createInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("potion-gui.title"));
        }

        ItemStack blackPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
        ItemMeta bm = blackPane.getItemMeta();
        bm.setDisplayName("§r");
        blackPane.setItemMeta(bm);

        for (int i = 0; i < 54; i++) {
            inventory.setItem(i, blackPane);
        }

        ItemStack back = new ItemStack(Material.IRON_DOOR);
        ItemMeta backM = back.getItemMeta();
        backM.setDisplayName(ConfigManager.getString("potion-gui.back-to-menu"));
        back.setItemMeta(backM);

        inventory.setItem(45, back);

        if (potionEffect == null) {
            PotionMeta potionMeta = (PotionMeta) toEdit.getItemMeta();
            List<PotionEffect> customEffects = potionMeta.getCustomEffects();
            if (customEffects != null) {
                for (PotionEffect customEffect : customEffects) {
                    if (customEffect.getType() == potionEffectType) {
                        potionEffect = customEffect;
                    }
                }
            }
            if (potionEffect == null) {
                potionEffect = new PotionEffect(potionEffectType, 0, 0, false, true);
            }
        }

        ItemStack stack = new ItemStack(Material.POTION, 1, (byte) 0);
        PotionMeta meta = (PotionMeta) stack.getItemMeta();
        meta.addCustomEffect(potionEffect, true);
        stack.setItemMeta(meta);

        inventory.setItem(13, stack);

        ItemStack durDisplay = new ItemStack(Material.WATCH);
        ItemMeta durMeta = durDisplay.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ConfigManager.getString("potion-gui.time-in-ticks").replace("<time>", String.valueOf(potionEffect.getDuration())));
        lore.add(ConfigManager.getString("potion-gui.time-in-minutes").replace("<time>", formatTicks(potionEffect.getDuration())));
        durMeta.setLore(lore);
        durMeta.setDisplayName(ConfigManager.getString("potion-gui.duration-display-name"));
        durDisplay.setItemMeta(durMeta);

        inventory.setItem(22, durDisplay);

        ItemStack ampDisplay = new ItemStack(Material.REDSTONE_BLOCK);
        ItemMeta ampMeta = ampDisplay.getItemMeta();
        ampMeta.setDisplayName(ConfigManager.getString("potion-gui.amplifier-display-name"));
        lore.clear();
        lore.add(ConfigManager.getString("potion-gui.amplifier-display-amount").replace("<amplifier>", String.valueOf(potionEffect.getAmplifier())));
        ampMeta.setLore(lore);
        ampDisplay.setItemMeta(ampMeta);

        inventory.setItem(31, ampDisplay);

        ItemStack ampMod = new ItemStack(Material.REDSTONE);
        ItemMeta ampModMeta = ampMod.getItemMeta();
        for (int i = 0; i < modifyRanges.length; i++) {
            ampModMeta.setDisplayName(ConfigManager.getString("potion-gui.amplifier-modify-amount-add").replace("<amount>", String.valueOf(modifyRanges[i])));
            ampMod.setAmount(modifyRanges[i]);
            ampMod.setItemMeta(ampModMeta);
            inventory.setItem(32 + i, ampMod);
            ampModMeta.setDisplayName(ConfigManager.getString("potion-gui.amplifier-modify-amount-decrease").replace("<amount>", String.valueOf(modifyRanges[i])));
            ampMod.setItemMeta(ampModMeta);
            inventory.setItem(30 - i, ampMod);
        }

        ItemStack timeMod = new ItemStack(Material.GOLD_INGOT);
        ItemMeta timeMeta = timeMod.getItemMeta();
        for (int i = 0; i < timeRanges.length; i++) {
            timeMeta.setDisplayName(ConfigManager.getString("potion-gui.time-modify-amount-add")
                    .replace("<time_ticks>", String.valueOf(timeRanges[i]))
                    .replace("<time>", String.valueOf(formatTicks(timeRanges[i]))));
            timeMod.setAmount(timeRanges[i]);
            lore.clear();
            lore.add(ConfigManager.getString("potion-gui.time-modify-amount-shift-add")
                    .replace("<time_ticks>", String.valueOf(timeRanges[i] * 10))
                    .replace("<time>", String.valueOf(formatTicks(timeRanges[i] * 10))));
            timeMeta.setLore(lore);
            timeMod.setItemMeta(timeMeta);
            inventory.setItem(23 + i, timeMod);
            timeMeta.setDisplayName(ConfigManager.getString("potion-gui.time-modify-amount-decrease")
                    .replace("<time_ticks>", String.valueOf(timeRanges[i]))
                    .replace("<time>", String.valueOf(formatTicks(timeRanges[i]))));
            timeMod.setAmount(timeRanges[i]);
            lore.clear();
            lore.add(ConfigManager.getString("potion-gui.time-modify-amount-shift-decrease")
                    .replace("<time_ticks>", String.valueOf(timeRanges[i] * 10))
                    .replace("<time>", String.valueOf(formatTicks(timeRanges[i] * 10))));
            timeMeta.setLore(lore);
            timeMod.setItemMeta(timeMeta);
            inventory.setItem(21 - i, timeMod);
        }

        ItemStack particleSwitch = new ItemStack(potionEffect.hasParticles() ? Material.GLOWSTONE : Material.STONE);
        ItemMeta particleMeta = particleSwitch.getItemMeta();
        particleMeta.setDisplayName(ConfigManager.getString("potion-gui.particle-switch"));
        lore.clear();
        lore.add(ConfigManager.getString("potion-gui.particles-" + String.valueOf(potionEffect.hasParticles())));
        particleMeta.setLore(lore);
        particleSwitch.setItemMeta(particleMeta);

        inventory.setItem(40, particleSwitch);

    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        switch (event.getRawSlot()) {
            case 45:
                dispose(true);
                break;
            case 23:
            case 24:
            case 25:
            case 26:
                incrDuration(event.isShiftClick(), event.getRawSlot());
                break;
            case 21:
            case 20:
            case 19:
            case 18:
                decrDuration(event.isShiftClick(), event.getRawSlot());
                break;
            case 32:
            case 33:
            case 34:
                incrAmp(event.getRawSlot());
                break;
            case 30:
            case 29:
            case 28:
                decrAmp(event.getRawSlot());
                break;
            case 40:
                toggleParticles();
                break;
        }
    }

    private void toggleParticles() {
        potionEffect = new PotionEffect(potionEffectType, potionEffect.getDuration(), potionEffect.getAmplifier(), false, !potionEffect.hasParticles());
        applyPotionEffectToItemStack();
        createInventory();
    }

    private void incrDuration(boolean isShift, int rawSlot) {
        rawSlot = Math.abs(rawSlot - 23);
        potionEffect = new PotionEffect(potionEffectType, potionEffect.getDuration() + timeRanges[rawSlot] * (isShift ? 10 : 1), potionEffect.getAmplifier());
        applyPotionEffectToItemStack();
        createInventory();
    }

    private void decrDuration(boolean isShift, int rawSlot) {
        rawSlot = Math.abs(21 - rawSlot);
        int nuDuration = potionEffect.getDuration() - timeRanges[rawSlot] * (isShift ? 10 : 1);
        if (nuDuration < 0) {
            return;
        }
        potionEffect = new PotionEffect(potionEffectType, nuDuration, potionEffect.getAmplifier());
        applyPotionEffectToItemStack();
        createInventory();
    }

    private void incrAmp(int rawSlot) {
        rawSlot = Math.abs(rawSlot - 32);
        potionEffect = new PotionEffect(potionEffectType, potionEffect.getDuration(), potionEffect.getAmplifier() + modifyRanges[rawSlot]);
        applyPotionEffectToItemStack();
        createInventory();
    }

    private void decrAmp(int rawSlot) {
        rawSlot = Math.abs(30 - rawSlot);
        int nuAmp = potionEffect.getAmplifier() - modifyRanges[rawSlot];
        if (nuAmp < 0) {
            return;
        }
        potionEffect = new PotionEffect(potionEffectType, potionEffect.getDuration(), nuAmp);
        applyPotionEffectToItemStack();
        createInventory();
    }

    private void applyPotionEffectToItemStack() {
        PotionMeta meta = (PotionMeta) toEdit.getItemMeta();
        meta.removeCustomEffect(potionEffectType);
        meta.addCustomEffect(potionEffect, true);
        toEdit.setItemMeta(meta);
        player.getInventory().setItemInMainHand(toEdit);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        dispose(true);
    }

    private void dispose(boolean resume) {
        HandlerList.unregisterAll(this);
        if (resume) {
            Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
                resumable.resume(toEdit);
                return null;
            });
        }
    }

    private String formatTicks(int ticks) {
        int minutes = 0;
        int seconds = 0;
        while (ticks >= 20) {
            while (ticks >= 20 * 60) {
                ticks -= 20 * 60;
                ++minutes;
            }
            if (ticks >= 20) {
                ticks -= 20;
                ++seconds;
            }
        }
        return (minutes >= 10 ? minutes : "0" + minutes) + ":" + (seconds >= 10 ? seconds : "0" + seconds);
    }

    @Override
    public void resume(Object... params) {
        createInventory();
        updateItemStack();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    @Override
    public void updateItemStack() {
        toEdit = player.getInventory().getItemInMainHand();
    }
}
