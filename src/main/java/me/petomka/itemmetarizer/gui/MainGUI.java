package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.KnowledgeBookMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.inventory.meta.SpawnEggMeta;

import java.util.UUID;

/**
 * Created by Benedikt on 23.08.2017.
 */
public class MainGUI implements Listener, Resumable {

    private Inventory inventory;

    private Player player;

    private ItemStack toEdit;

    public static ItemStack getBlackGlassPane() {
        ItemStack blackPane = new ItemStack(Material.STAINED_GLASS_PANE);
        ItemMeta blackPaneMeta = blackPane.getItemMeta();
        blackPaneMeta.setDisplayName("§r");
        blackPane.setDurability((short) 15);
        blackPane.setItemMeta(blackPaneMeta);
        return blackPane;
    }

    public static ItemStack getBlueGlassPane() {
        ItemStack blackPane = new ItemStack(Material.STAINED_GLASS_PANE);
        ItemMeta blackPaneMeta = blackPane.getItemMeta();
        blackPaneMeta.setDisplayName("§r");
        blackPane.setDurability((short) 11);
        blackPane.setItemMeta(blackPaneMeta);
        return blackPane;
    }

    public MainGUI(Player player, ItemStack toEdit) {
        this.player = player;
        this.toEdit = toEdit;
        Bukkit.getPluginManager().registerEvents(this, Main.main);
        createInventory();

        player.openInventory(inventory);
    }

    @Override
    public void resume(Object... params) {
        if (params.length > 0) {
            Property property = Property.valueOf(String.valueOf(params[0]));
            if (property == Property.name) {
                setName((String) params[1]);
            }
        }
        updateItemStack();
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    public void updateItemStack() {
        toEdit = player.getInventory().getItemInMainHand();
    }

    enum Property {
        name(Material.ANVIL, 10),
        enchants(Material.ENCHANTMENT_TABLE, 12),
        attributes(Material.COMMAND, 14),
        flags(Material.COMPASS, 16),
        lore(Material.BOOK_AND_QUILL, 28),
        unbreakable(Material.BEDROCK, 30),
        durability(Material.IRON_SWORD, 32),
        //reset(Material.BARRIER, 34),
        amount(Material.MELON_SEEDS, 34),
        save(Material.RECORD_8, 45),
        saved_list(Material.CHEST, 46);
        public Material material;
        final int slot;

        Property(Material material, int slot) {
            this.material = material;
            this.slot = slot;
        }
    }

    enum SpecialMeta {
        bannerMeta(Material.BANNER),
        bookMeta(Material.BOOK_AND_QUILL),
        enchantmentStorageMeta(Material.ENCHANTED_BOOK),
        fireworkMeta(Material.FIREWORK),
        knowledgeBookMeta(Material.KNOWLEDGE_BOOK),
        leatherArmorMeta(Material.INK_SACK),
        mapMeta(Material.MAP),
        potionMeta(Material.BREWING_STAND_ITEM),
        skullMeta(Material.SKULL_ITEM),
        spawnEggMeta(Material.MONSTER_EGG);
        private Material iconMaterial;

        SpecialMeta(Material iconMaterial) {
            this.iconMaterial = iconMaterial;
        }

        public static boolean isInstanceOf(SpecialMeta meta, Object test) {
            switch (meta) {
                case bannerMeta:
                    return test instanceof BannerMeta;
                case bookMeta:
                    return test instanceof BookMeta;
                case enchantmentStorageMeta:
                    return test instanceof EnchantmentStorageMeta;
                case fireworkMeta:
                    return test instanceof FireworkMeta;
                case knowledgeBookMeta:
                    return test instanceof KnowledgeBookMeta;
                case leatherArmorMeta:
                    return test instanceof LeatherArmorMeta;
                case mapMeta:
                    return test instanceof MapMeta;
                case potionMeta:
                    return test instanceof PotionMeta;
                case skullMeta:
                    return test instanceof SkullMeta;
                case spawnEggMeta:
                    return test instanceof SpawnEggMeta;
            }
            return false;
        }
    }

    private SpecialMeta specialMeta = null;

    private void createInventory() {
        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("main-gui.title"));


        {
            ItemStack blackPane = getBlackGlassPane();

            for (int i = 0; i < 54; i++) {
                inventory.setItem(i, blackPane);
            }
        }

        for (Property property : Property.values()) {
            ItemStack editProperty;
            if (property == Property.unbreakable) {
                editProperty = new ItemStack(toEdit.getItemMeta().isUnbreakable() ? Material.BEDROCK : Material.DIRT);
            } else {
                editProperty = new ItemStack(property.material);
            }
            ItemMeta editNameMeta = editProperty.getItemMeta();
            editNameMeta.setDisplayName(ConfigManager.getString("main-gui.edit-" + property.name()));
            editProperty.setItemMeta(editNameMeta);
            inventory.setItem(property.slot, editProperty);
        }
        ItemStack icon = null;
        for (SpecialMeta specialMeta : SpecialMeta.values()) {
            if (SpecialMeta.isInstanceOf(specialMeta, toEdit.getItemMeta())) {
                icon = new ItemStack(specialMeta.iconMaterial);
                ItemMeta iconMeta = icon.getItemMeta();
                iconMeta.setDisplayName(ConfigManager.getString("main-gui.edit-special"));
                icon.setItemMeta(iconMeta);
                this.specialMeta = specialMeta;
                break;
            }
        }
        if (icon == null) {
            icon = new ItemStack(Material.STAINED_GLASS_PANE);
            ItemMeta iconMeta = icon.getItemMeta();
            iconMeta.setDisplayName(ConfigManager.getString("main-gui.no-special"));
            icon.setDurability((short) 14);
            icon.setItemMeta(iconMeta);
        }
        inventory.setItem(49, icon);
    }

    private void dispose() {
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        int s = event.getRawSlot();
        if (s == Property.name.slot) {
            new ChatConfigurator(player, Property.name, this, ChatConfigurator.VALUE_TYPE.STRING);
        } else if (s == Property.lore.slot) {
            new LoreGUI(player, toEdit, this);
        } else if (s == Property.enchants.slot) {
            new EnchantmentsGUI(player, toEdit, this);
        } else if (s == Property.attributes.slot) {
            new AttributesGUI(toEdit, player, this);
        } else if (s == Property.unbreakable.slot) {
            toggleUnbreakable();
        } else if (s == Property.durability.slot) {
            new DamageGUI(player, toEdit, this);
        } else if (s == Property.flags.slot) {
            new ItemFlagGUI(player, this, toEdit);
        } else if (s == Property.amount.slot) {
//            ItemStack stack = new ItemStack(toEdit.getType());
//            stack.setAmount(toEdit.getAmount());
//            stack.setDurability(toEdit.getDurability());
//            player.getInventory().setItemInMainHand(stack);
//            updateItemStack();
            new AmountGUI(player, toEdit, this);
        } else if(specialMeta != null && s == 49) {
            newSpecialGUI();
        } else if(s == Property.save.slot) {
            saveItem();
            player.sendMessage(ConfigManager.getString("main-gui.item-saved"));
        } else if(s == Property.saved_list.slot) {
            new SavedItemsGUI(player, this);
        }
    }

    private void newSpecialGUI() {
        switch (specialMeta) {
            case potionMeta:
                new PotionEffectGUI(player, toEdit, this);
                break;
            case leatherArmorMeta:
                new ColorGUI(toEdit, player, this);
                break;
            case skullMeta:
                new SkullOwnerGUI(player, this, toEdit);
                break;
            case enchantmentStorageMeta:
                new EnchantmentsGUI(player, toEdit, this, true);
                break;
        }
    }

    private void toggleUnbreakable() {
        ItemMeta meta = toEdit.getItemMeta();
        meta.setUnbreakable(!meta.isUnbreakable());
        toEdit.setItemMeta(meta);

        ItemStack toggleUnbreakable = new ItemStack(meta.isUnbreakable() ? Material.BEDROCK : Material.DIRT);
        ItemMeta uMeta = toggleUnbreakable.getItemMeta();
        uMeta.setDisplayName(ConfigManager.getString("main-gui.edit-unbreakable"));
        toggleUnbreakable.setItemMeta(uMeta);

        inventory.setItem(Property.unbreakable.slot, toggleUnbreakable);
    }

    private void saveItem() {
        Main.main.getItemsConfig().set(UUID.randomUUID().toString(), toEdit);
        Main.main.saveItemConfig();
    }

    private void setName(String name) {
        ItemMeta meta = toEdit.getItemMeta();
        meta.setDisplayName(name);
        toEdit.setItemMeta(meta);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory().equals(inventory)) {
            dispose();
        }
    }

}
