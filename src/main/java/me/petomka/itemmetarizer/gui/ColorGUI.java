package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;

import java.util.ArrayList;

public class ColorGUI implements Listener {

    private static class ColorMetaWrapper {

        private ItemStack itemStack;

        public boolean isValid() {
            return itemStack.getItemMeta() instanceof LeatherArmorMeta || itemStack.getItemMeta() instanceof PotionMeta;
        }

        public ColorMetaWrapper(ItemStack stack) {
            this.itemStack = stack;
        }

        public ItemStack setColor(Color color) {
            if (itemStack.getItemMeta() instanceof LeatherArmorMeta) {
                LeatherArmorMeta meta = (LeatherArmorMeta) itemStack.getItemMeta();
                meta.setColor(color);
                itemStack.setItemMeta(meta);
            } else if (itemStack.getItemMeta() instanceof PotionMeta) {
                PotionMeta meta = (PotionMeta) itemStack.getItemMeta();
                meta.setColor(color);
                itemStack.setItemMeta(meta);
            }
            return itemStack;
        }

        public Color getColor() {
            Color color = null;
            if (itemStack.getItemMeta() instanceof LeatherArmorMeta) {
                LeatherArmorMeta meta = (LeatherArmorMeta) itemStack.getItemMeta();
                color = meta.getColor();
            } else if (itemStack.getItemMeta() instanceof PotionMeta) {
                PotionMeta meta = (PotionMeta) itemStack.getItemMeta();
                color = meta.getColor();
            }
            if (color == null) {
                color = Color.fromRGB(0, 0, 0);
            }
            return color;
        }
    }

    private ColorMetaWrapper colorMetaWrapper = null;
    private Inventory inventory;
    private Player player;
    private ItemStack toEdit;
    private Resumable resumable;

    public ColorGUI(ItemStack toEdit, Player player, Resumable resumable) {
        colorMetaWrapper = new ColorMetaWrapper(toEdit);
        if (!colorMetaWrapper.isValid()) {
            resumable.resume();
            return;
        }
        this.player = player;
        this.toEdit = toEdit;
        this.resumable = resumable;
        createInventory();
        player.openInventory(this.inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private final int amounts[] = new int[]{1, 8, 64};

    private void createInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("color-gui.title"));
        }

        ArrayList<String> lore = new ArrayList<>();

        ItemStack blackPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 15);
        ItemMeta blackMeta = blackPane.getItemMeta();
        blackMeta.setDisplayName("§r");
        blackPane.setItemMeta(blackMeta);

        for (int i = 0; i < 54; i++) {
            inventory.setItem(i, blackPane);
        }

        ItemStack red = new ItemStack(Material.INK_SACK, 1, (byte) 1);
        ItemMeta redMeta = red.getItemMeta();
        redMeta.setDisplayName(ConfigManager.getString("color-gui.red-display"));

        ItemStack green = new ItemStack(Material.INK_SACK, 1, (byte) 2);
        ItemMeta greenMeta = green.getItemMeta();
        greenMeta.setDisplayName(ConfigManager.getString("color-gui.green-display"));

        ItemStack blue = new ItemStack(Material.INK_SACK, 1, (byte) 4);
        ItemMeta blueMeta = blue.getItemMeta();
        blueMeta.setDisplayName(ConfigManager.getString("color-gui.blue-display"));

        for (int i = 0; i < 3; i++) {
            lore.clear();
            lore.add(ConfigManager.getString("color-gui.add-red").replace("<amount>", String.valueOf(amounts[i])));
            redMeta.setLore(lore);
            red.setItemMeta(redMeta);
            red.setAmount(amounts[i]);

            lore.clear();
            lore.add(ConfigManager.getString("color-gui.add-green").replace("<amount>", String.valueOf(amounts[i])));
            greenMeta.setLore(lore);
            green.setItemMeta(greenMeta);
            green.setAmount(amounts[i]);

            lore.clear();
            lore.add(ConfigManager.getString("color-gui.add-blue").replace("<amount>", String.valueOf(amounts[i])));
            blueMeta.setLore(lore);
            blue.setItemMeta(blueMeta);
            blue.setAmount(amounts[i]);

            inventory.setItem(14 + i, red);
            inventory.setItem(23 + i, green);
            inventory.setItem(32 + i, blue);
        }

        for (int i = 0; i < 3; i++) {
            lore.clear();
            lore.add(ConfigManager.getString("color-gui.subtract-red").replace("<amount>", String.valueOf(amounts[i])));
            redMeta.setLore(lore);
            red.setItemMeta(redMeta);
            red.setAmount(amounts[i]);

            lore.clear();
            lore.add(ConfigManager.getString("color-gui.subtract-green").replace("<amount>", String.valueOf(amounts[i])));
            greenMeta.setLore(lore);
            green.setItemMeta(greenMeta);
            green.setAmount(amounts[i]);

            lore.clear();
            lore.add(ConfigManager.getString("color-gui.subtract-blue").replace("<amount>", String.valueOf(amounts[i])));
            blueMeta.setLore(lore);
            blue.setItemMeta(blueMeta);
            blue.setAmount(amounts[i]);

            inventory.setItem(12 - i, red);
            inventory.setItem(21 - i, green);
            inventory.setItem(30 - i, blue);
        }

        ItemStack back = new ItemStack(Material.IRON_DOOR);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName(ConfigManager.getString("color-gui.back"));
        back.setItemMeta(backMeta);

        inventory.setItem(45, back);

        ItemStack midDisplay = toEdit.clone();
        ItemMeta midMeta = toEdit.getItemMeta();
        lore.clear();
        for(String s : Main.main.getConfig().getStringList("color-gui.color-display")) {
            s = ChatColor.translateAlternateColorCodes('&', s)
                    .replace("<red>", String.valueOf(colorMetaWrapper.getColor().getRed()))
                    .replace("<green>", String.valueOf(colorMetaWrapper.getColor().getGreen()))
                    .replace("<blue>", String.valueOf(colorMetaWrapper.getColor().getBlue()))
                    .replace("<hex>", toFullHexString(Integer.toHexString(colorMetaWrapper.getColor().asRGB())));
            lore.add(s);
        }
        midMeta.setLore(lore);
        midDisplay.setItemMeta(midMeta);

        inventory.setItem(22, midDisplay);
    }

    private String toFullHexString(String entry) {
        int append = 6 - entry.length();
        StringBuilder entryBuilder = new StringBuilder(entry);
        for(int i = 0; i < append; i++) {
            entryBuilder.insert(0, "0");
        }
        return entryBuilder.toString();
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        switch (event.getRawSlot()) {
            case 45:
                dispose();
                break;
            case 14:
                addColor(1, 0, 0);
                break;
            case 15:
                addColor(8, 0, 0);
                break;
            case 16:
                addColor(64, 0, 0);
                break;
            case 23:
                addColor(0, 1, 0);
                break;
            case 24:
                addColor(0, 8, 0);
                break;
            case 25:
                addColor(0, 64, 0);
                break;
            case 32:
                addColor(0, 0, 1);
                break;
            case 33:
                addColor(0, 0, 8);
                break;
            case 34:
                addColor(0, 0, 64);
                break;
            case 10:
                subtractColor(64, 0, 0);
                break;
            case 11:
                subtractColor(8, 0, 0);
                break;
            case 12:
                subtractColor(1, 0, 0);
                break;
            case 19:
                subtractColor(0, 64, 0);
                break;
            case 20:
                subtractColor(0, 8, 0);
                break;
            case 21:
                subtractColor(0, 1, 0);
                break;
            case 28:
                subtractColor(0, 0, 64);
                break;
            case 29:
                subtractColor(0, 0, 8);
                break;
            case 30:
                subtractColor(0, 0, 1);
                break;
        }
    }

    private void addColor(int r, int g, int b) {
        r += colorMetaWrapper.getColor().getRed();
        g += colorMetaWrapper.getColor().getGreen();
        b += colorMetaWrapper.getColor().getBlue();
        if (r > 255) {
            r = 255;
        }
        if (g > 255) {
            g = 255;
        }
        if (b > 255) {
            b = 255;
        }
        toEdit = colorMetaWrapper.setColor(Color.fromRGB(r, g, b));
        updatePlayersItem();
        createInventory();
    }

    private void subtractColor(int r, int g, int b) {
        r = colorMetaWrapper.getColor().getRed() - r;
        g = colorMetaWrapper.getColor().getGreen() - g;
        b = colorMetaWrapper.getColor().getBlue() - b;
        if (r < 0) {
            r = 0;
        }
        if (g < 0) {
            g = 0;
        }
        if (b < 0) {
            b = 0;
        }
        toEdit = colorMetaWrapper.setColor(Color.fromRGB(r, g, b));
        updatePlayersItem();
        createInventory();
    }

    private void updatePlayersItem() {
        player.getInventory().setItemInMainHand(toEdit);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory().equals(inventory)) {
            dispose();
        }
    }

    private void dispose() {
        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
            resumable.resume();
            return null;
        });
    }

}
