package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Benedikt on 26.08.2017.
 */
public class EnchantmentsGUI implements Listener, Resumable {

    private ItemStack toEdit;

    private Player player;

    private Resumable resumable;

    private Inventory inventory;

    private boolean enchantStorage;

    public EnchantmentsGUI(Player player, ItemStack itemStack, Resumable resumable, boolean enchantStorage) {
        this.enchantStorage = enchantStorage;
        this.player = player;
        this.toEdit = itemStack;
        this.resumable = resumable;
        Bukkit.getPluginManager().registerEvents(this, Main.main);
        createInventory();
        player.openInventory(inventory);
    }

    public EnchantmentsGUI(Player player, ItemStack toEdit, Resumable resumable) {
        this(player, toEdit, resumable, false);
    }

    public static class EnchantmentContainer {

        private boolean allEnchants = false;

        private List<Enchantment> enchantments = new ArrayList<>();

        public EnchantmentContainer(boolean allEnchants) {
            this.allEnchants = allEnchants;
        }

        public EnchantmentContainer(Enchantment[]... enchantments) {
            for (Enchantment[] enchantments1 : enchantments) {
                this.enchantments.addAll(Arrays.asList(enchantments1));
            }
        }

        public List<Enchantment> getEnchantments() {
            if (allEnchants) {
                List<Enchantment> enchs = new ArrayList<>();
                for (TypeEnchant enchant : TypeEnchant.values()) {
                    if (enchant == TypeEnchant.Curse) {
                        continue;
                    }
                    enchs.addAll(Arrays.asList(enchant.enchantments));
                }
                return enchs;
            }
            return enchantments;
        }
    }

    enum TypeEnchant {
        Armor(Material.DIAMOND_CHESTPLATE, ConfigManager.getString("enchant-gui.armor"), Enchantment.PROTECTION_ENVIRONMENTAL,
                Enchantment.PROTECTION_FALL, Enchantment.PROTECTION_FIRE, Enchantment.PROTECTION_PROJECTILE, Enchantment.WATER_WORKER,
                Enchantment.OXYGEN, Enchantment.PROTECTION_EXPLOSIONS, Enchantment.DEPTH_STRIDER, Enchantment.FROST_WALKER,
                Enchantment.THORNS),
        Weapon(Material.DIAMOND_SWORD, ConfigManager.getString("enchant-gui.weapon"), Enchantment.DAMAGE_ARTHROPODS,
                Enchantment.FIRE_ASPECT, Enchantment.KNOCKBACK, Enchantment.LOOT_BONUS_MOBS, Enchantment.DAMAGE_ALL,
                Enchantment.SWEEPING_EDGE, Enchantment.DAMAGE_UNDEAD),
        Tool(Material.DIAMOND_SPADE, ConfigManager.getString("enchant-gui.tool"), Enchantment.DIG_SPEED,
                Enchantment.LOOT_BONUS_BLOCKS, Enchantment.SILK_TOUCH),
        Bow(Material.BOW, ConfigManager.getString("enchant-gui.bow"), Enchantment.ARROW_FIRE, Enchantment.ARROW_INFINITE,
                Enchantment.ARROW_DAMAGE, Enchantment.ARROW_KNOCKBACK),
        Fishing_Rod(Material.FISHING_ROD, ConfigManager.getString("enchant-gui.fishing_rod"), Enchantment.LUCK,
                Enchantment.LURE),
        Universal(Material.NETHER_STAR, ConfigManager.getString("enchant-gui.universal"), Enchantment.MENDING,
                Enchantment.DURABILITY),
        Curse(Material.SKULL_ITEM, ConfigManager.getString("enchant-gui.curse"), Enchantment.BINDING_CURSE,
                Enchantment.VANISHING_CURSE);
        public Enchantment[] enchantments;
        public String iconName;
        public Material iconMaterial;

        TypeEnchant(Material iconMaterial, String iconName, Enchantment... enchantments) {
            this.enchantments = enchantments;
            this.iconName = iconName;
            this.iconMaterial = iconMaterial;
        }
    }

    int page = 0;

    private void createInventory() {
        if (inventory == null) {
            inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("enchant-gui.title"));
        }
        fillInventoryWithBlackPanes();

        String editEnchant = ConfigManager.getString("enchant-gui.edit-enchant");
        String removeEnchants = ConfigManager.getString("enchant-gui.remove-enchant");

        int line = 0;
        int column = 1;
        int internalPage = 0;

        for (TypeEnchant typeEnchant : TypeEnchant.values()) {
            if (line == 5) {
                internalPage += 1;
                line = 0;
            }
            if (internalPage == page) {
                ItemStack symbol = new ItemStack(typeEnchant.iconMaterial);
                ItemMeta symbolMeta = symbol.getItemMeta();
                symbolMeta.setDisplayName(typeEnchant.iconName);
                List<String> lore = new ArrayList<>();
                lore.add(ConfigManager.getString("enchant-gui.add-group"));
                lore.add(ConfigManager.getString("enchant-gui.remove-group"));
                symbolMeta.setLore(lore);
                symbol.setItemMeta(symbolMeta);
                inventory.setItem(line * 9, symbol);
            }
            for (Enchantment enchantment : typeEnchant.enchantments) {
                if (line == 5) {
                    internalPage += 1;
                    line = 0;
                }
                if (internalPage == page) {
                    ItemStack enchIcon = new ItemStack(Material.ENCHANTED_BOOK);
                    EnchantmentStorageMeta meta = (EnchantmentStorageMeta) enchIcon.getItemMeta();
                    meta.setDisplayName("§r" + makeSenseOutOfEnum(enchantment.getName()));
                    List<String> lore = new ArrayList<>();
                    lore.add(editEnchant);
                    lore.add(removeEnchants);
                    meta.setLore(lore);
                    enchIcon.setItemMeta(meta);
                    inventory.setItem(line * 9 + column, enchIcon);
                }
                ++column;
                if (column == 9) {
                    line += 1;
                    column = 1;
                }
            }
            line += 1;
            column = 1;
        }

        ItemStack exit = new ItemStack(Material.IRON_DOOR);
        ItemMeta exitMeta = exit.getItemMeta();
        exitMeta.setDisplayName(ConfigManager.getString("enchant-gui.exit-to-menu"));
        exit.setItemMeta(exitMeta);
        inventory.setItem(45, exit);

        ItemStack addAll = new ItemStack(Material.ENCHANTMENT_TABLE);
        ItemMeta addAllMeta = addAll.getItemMeta();
        addAllMeta.setDisplayName(ConfigManager.getString("enchant-gui.add-all"));
        addAll.setItemMeta(addAllMeta);
        inventory.setItem(48, addAll);

        ItemStack removeAll = new ItemStack(Material.BARRIER);
        ItemMeta removeMeta = removeAll.getItemMeta();
        removeMeta.setDisplayName(ConfigManager.getString("enchant-gui.remove-all"));
        removeAll.setItemMeta(removeMeta);
        inventory.setItem(50, removeAll);

        ItemStack pageDown = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta pageDownMeta = pageDown.getItemMeta();
        pageDownMeta.setDisplayName(ConfigManager.getString("enchant-gui.prev-page"));
        pageDown.setItemMeta(pageDownMeta);
        inventory.setItem(52, pageDown);

        ItemStack pageUp = new ItemStack(Material.GHAST_TEAR);
        ItemMeta pageUpMeta = pageUp.getItemMeta();
        pageUpMeta.setDisplayName(ConfigManager.getString("enchant-gui.next-page"));
        pageUp.setItemMeta(pageUpMeta);
        inventory.setItem(53, pageUp);
    }

    public void resume(Object... params) {
        updateItemStack();
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    public void updateItemStack() {
        toEdit = player.getInventory().getItemInMainHand();
    }

    private String makeSenseOutOfEnum(String enumName) {
        String[] parts = enumName.split("_");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.length; i++) {
            String s = parts[i].toLowerCase();
            char f = s.charAt(0);
            f = Character.toUpperCase(f);
            s = f + s.substring(1, s.length());
            sb.append(s);
            if (i + 1 < parts.length) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    private Enchantment makeEnumOutOfSense(String senseName) {
        senseName = senseName.replace(" ", "_").toUpperCase().substring(2); //substring to get rid of the "§r"
        return Enchantment.getByName(senseName);
    }

    private void fillInventoryWithBlackPanes() {
        ItemStack blackPane = MainGUI.getBlackGlassPane();
        ItemStack bluePane = MainGUI.getBlueGlassPane();

        for (int i = 0; i < 54; i++) {
            if(i >= 45) {
                inventory.setItem(i, bluePane);
                continue;
            }
            inventory.setItem(i, blackPane);
        }


    }

    private void dispose(boolean resume) {
        HandlerList.unregisterAll(this);
        if (resume) {
            Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
                resumable.resume(MainGUI.Property.enchants);
                return null;
            });
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if (event.getRawSlot() == 53) {
            if (page < 1) {
                page += 1;
                createInventory();
                player.updateInventory();
            }
        } else if (event.getRawSlot() == 52) {
            if (page > 0) {
                page -= 1;
                createInventory();
                player.updateInventory();
            }
        } else if (event.getRawSlot() == 45) {
            dispose(true);
        } else if (event.getRawSlot() == 48) {
            //add all
            dispose(false);
            new EnchantModifyGUI(player, this, toEdit, event.getCurrentItem(), new EnchantmentContainer(true), enchantStorage);
        } else if (event.getRawSlot() == 50) {
            //remove all
            removeAllEnchants();
        } else {
            if (event.getCurrentItem() == null || event.getRawSlot() > 53 || !event.getCurrentItem().hasItemMeta()) {
                return;
            }
            {
                Enchantment enchantment = makeEnumOutOfSense(event.getCurrentItem().getItemMeta().getDisplayName());
                if (enchantment != null) {
                    if (event.getClick() == ClickType.LEFT) {
                        dispose(false);
                        new EnchantModifyGUI(player, this, toEdit, event.getCurrentItem(), new EnchantmentContainer(new Enchantment[]{enchantment}), enchantStorage);
                        return;
                    } else if (event.getClick() == ClickType.RIGHT) {
                        if(enchantStorage) {
                            EnchantmentStorageMeta storageMeta = (EnchantmentStorageMeta) toEdit.getItemMeta();
                            if(storageMeta.hasStoredEnchant(enchantment)) {
                                storageMeta.removeStoredEnchant(enchantment);
                                toEdit.setItemMeta(storageMeta);
                                return;
                            }
                        }
                        ItemMeta meta = toEdit.getItemMeta();
                        if (meta.hasEnchant(enchantment)) {
                            meta.removeEnchant(enchantment);
                            toEdit.setItemMeta(meta);
                        }
                    }
                }
            }
            for (TypeEnchant enchant : TypeEnchant.values()) {
                if (event.getCurrentItem().getType() == enchant.iconMaterial) {
                    if (event.getClick() == ClickType.LEFT) {
                        dispose(false);
                        new EnchantModifyGUI(player, this, toEdit, event.getCurrentItem(), new EnchantmentContainer(enchant.enchantments), enchantStorage);
                    } else if (event.getClick() == ClickType.RIGHT) {
                        if(enchantStorage) {
                            EnchantmentStorageMeta storageMeta = (EnchantmentStorageMeta) toEdit.getItemMeta();
                            for(Enchantment enchantment : enchant.enchantments) {
                                if(storageMeta.hasStoredEnchant(enchantment)) {
                                    storageMeta.removeStoredEnchant(enchantment);
                                }
                            }
                            toEdit.setItemMeta(storageMeta);
                            continue;
                        }
                        ItemMeta meta = toEdit.getItemMeta();
                        for (Enchantment enchantment : enchant.enchantments) {
                            if (meta.hasEnchant(enchantment)) {
                                meta.removeEnchant(enchantment);
                            }
                        }
                        toEdit.setItemMeta(meta);
                    }
                }
            }
        }
    }

    private void removeAllEnchants() {
        if(enchantStorage) {
            EnchantmentStorageMeta storageMeta = (EnchantmentStorageMeta) toEdit.getItemMeta();
            for(Enchantment enchantment : storageMeta.getStoredEnchants().keySet()) {
                storageMeta.removeStoredEnchant(enchantment);
            }
            toEdit.setItemMeta(storageMeta);
            return;
        }
        ItemMeta meta = toEdit.getItemMeta();
        for (Enchantment enchantment : meta.getEnchants().keySet()) {
            meta.removeEnchant(enchantment);
        }
        toEdit.setItemMeta(meta);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        dispose(true);
    }

}
