package me.petomka.itemmetarizer.gui;

/**
 * Created by Benedikt on 24.08.2017.
 */
public interface Resumable {

    /**
     * Used to communicate between UIs and ChatConfigurator
     * @param params Depends on where the call is made from, first param is a property
     */
    void resume(Object... params);

    void updateItemStack();

}
