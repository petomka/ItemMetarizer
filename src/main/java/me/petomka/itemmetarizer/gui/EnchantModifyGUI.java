package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benedikt on 27.08.2017.
 */
public class EnchantModifyGUI implements Listener {

    private Player player;

    private Inventory inventory;

    private Resumable resumable;

    private ItemStack toEdit;
    private ItemStack enchantIcon;

    private EnchantmentsGUI.EnchantmentContainer enchantmentContainer;

    private boolean enchantStorage;

    public EnchantModifyGUI(Player player, Resumable resumable, ItemStack toEdit, ItemStack enchantIcon, EnchantmentsGUI.EnchantmentContainer enchantmentContainer, boolean enchantStorage) {
        this.player = player;
        this.resumable = resumable;
        this.toEdit = toEdit;
        this.enchantIcon = enchantIcon;
        this.enchantmentContainer = enchantmentContainer;
        this.enchantStorage = enchantStorage;
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void createInventory() {
        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("enchant-gui.title"));

        ItemStack blackPane = MainGUI.getBlackGlassPane();

        for(int i = 0; i < 45; i++) {
            inventory.setItem(i, blackPane);
        }

        ItemStack netherStar = new ItemStack(Material.NETHER_STAR);
        ItemMeta netherMeta = netherStar.getItemMeta();

        for(int a = 0; a < 2; a++) {
            for(int b = 0; b < 5; b++) {
                int amount = (b+1) * Math.max(1, (a > 0 ? 10 : 0));
                netherMeta.setDisplayName(ConfigManager.getString("enchant-gui.modify-amount").replace("<amount>", String.valueOf(amount)));
                List<String> lore = new ArrayList<>();
                lore.add(ConfigManager.getString("enchant-gui.modify-increase"));
                lore.add(ConfigManager.getString("enchant-gui.modify-decrease"));
                netherMeta.setLore(lore);
                netherStar.setItemMeta(netherMeta);
                netherStar.setAmount(amount);
                inventory.setItem((a+2)*9 + b + 2, netherStar);
            }
        }

        ItemMeta iconMeta = enchantIcon.getItemMeta();
        iconMeta.setLore(null);
        enchantIcon.setItemMeta(iconMeta);
        inventory.setItem(13, enchantIcon);

        ItemStack backToMenu = new ItemStack(Material.IRON_DOOR);
        ItemMeta backMeta = backToMenu.getItemMeta();
        backMeta.setDisplayName(ConfigManager.getString("enchant-gui.exit-to-menu"));
        backToMenu.setItemMeta(backMeta);
        inventory.setItem(45, backToMenu);

        ItemStack bluePane = MainGUI.getBlueGlassPane();
        for(int i = 46; i < 54; i++) {
            inventory.setItem(i, bluePane);
        }

    }

    private void dispose() {
        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
            resumable.resume();
            return null;
        });
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if(enchantStorage) {
            handleClickEnchantStorage(event);
        } else {
            handleClickNormal(event);
        }
    }

    private void handleClickNormal(InventoryClickEvent event) {
        if(event.getRawSlot() == 45) {
            dispose();
        } else {
            int row = event.getRawSlot()/9;
            int column = event.getRawSlot()%9;
            if(row < 2 || column < 2 || row > 3 || column > 6) {
                return;
            }
            int amount = (column-2+1) * Math.max(1, (row-2 > 0 ? 10 : 0));
            if(event.getClick() == ClickType.SHIFT_LEFT || event.getClick() == ClickType.SHIFT_RIGHT) {
                amount *= 10;
            }
            ItemMeta editMeta = toEdit.getItemMeta();
            if(event.getClick() == ClickType.SHIFT_LEFT || event.getClick() == ClickType.LEFT) {
                for (Enchantment enchantment : enchantmentContainer.getEnchantments()) {
                    int level = 0;
                    if(editMeta.hasEnchant(enchantment)) {
                        level = editMeta.getEnchantLevel(enchantment);
                    }
                    level += amount;
                    editMeta.removeEnchant(enchantment);
                    editMeta.addEnchant(enchantment, level, true);
                }
            } else  if(event.getClick() == ClickType.SHIFT_RIGHT || event.getClick() == ClickType.RIGHT) {
                for (Enchantment enchantment : enchantmentContainer.getEnchantments()) {
                    int level = 0;
                    if(editMeta.hasEnchant(enchantment)) {
                        level = editMeta.getEnchantLevel(enchantment);
                    }
                    level -= amount;
                    editMeta.removeEnchant(enchantment);
                    editMeta.addEnchant(enchantment, level, true);
                }
            }
            toEdit.setItemMeta(editMeta);
        }

    }

    private void handleClickEnchantStorage(InventoryClickEvent event) {
        if(event.getRawSlot() == 45) {
            dispose();
        } else {
            int row = event.getRawSlot()/9;
            int column = event.getRawSlot()%9;
            if(row < 2 || column < 2 || row > 3 || column > 6) {
                return;
            }
            int amount = (column-2+1) * Math.max(1, (row-2 > 0 ? 10 : 0));
            if(event.getClick() == ClickType.SHIFT_LEFT || event.getClick() == ClickType.SHIFT_RIGHT) {
                amount *= 10;
            }
            EnchantmentStorageMeta editMeta = (EnchantmentStorageMeta) toEdit.getItemMeta();
            if(event.getClick() == ClickType.SHIFT_LEFT || event.getClick() == ClickType.LEFT) {
                for (Enchantment enchantment : enchantmentContainer.getEnchantments()) {
                    int level = 0;
                    if(editMeta.hasStoredEnchant(enchantment)) {
                        level = editMeta.getStoredEnchantLevel(enchantment);
                    }
                    level += amount;
                    editMeta.removeStoredEnchant(enchantment);
                    editMeta.addStoredEnchant(enchantment, level, true);
                }
            } else  if(event.getClick() == ClickType.SHIFT_RIGHT || event.getClick() == ClickType.RIGHT) {
                for (Enchantment enchantment : enchantmentContainer.getEnchantments()) {
                    int level = 0;
                    if(editMeta.hasStoredEnchant(enchantment)) {
                        level = editMeta.getStoredEnchantLevel(enchantment);
                    }
                    level -= amount;
                    editMeta.removeStoredEnchant(enchantment);
                    editMeta.addStoredEnchant(enchantment, level, true);
                }
            }
            toEdit.setItemMeta(editMeta);
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        dispose();
    }

}
