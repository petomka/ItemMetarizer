package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class SavedItemsGUI implements Listener {

    private Player player;
    private Resumable resumable;

    private Inventory inventory;

    private int page;

    private ArrayList<ItemStack> itemStacks = new ArrayList<>();
    private ArrayList<String> itemKeys = new ArrayList<>();

    public SavedItemsGUI(Player player, @Nullable Resumable resumable) {
        this.player = player;
        this.resumable = resumable;
        loadSavedItems();
        createInventory(false);
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void loadSavedItems() {
        for (String key : Main.main.getItemsConfig().getKeys(false)) {
            Object o = Main.main.getItemsConfig().get(key);
            if (o instanceof ItemStack) {
                itemStacks.add((ItemStack) o);
                itemKeys.add(key);
            }
        }
    }

    private void createInventory(boolean registerListenerAndOpenInv) {
        dispose(false);

        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("saved-gui.title").replace("<page>", String.valueOf(page + 1)));

        if (registerListenerAndOpenInv) {
            player.openInventory(inventory);
            Bukkit.getPluginManager().registerEvents(this, Main.main);
        }

        ItemStack blackPane = MainGUI.getBlackGlassPane();
        for (int i = 0; i < 45; i++) {
            inventory.setItem(i, blackPane);
        }
        ItemStack bluePane = MainGUI.getBlueGlassPane();
        for (int i = 45; i < 54; i++) {
            inventory.setItem(i, bluePane);
        }

        ItemStack back = new ItemStack(Material.IRON_DOOR);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName(ConfigManager.getString("saved-gui.back"));
        back.setItemMeta(backMeta);

        inventory.setItem(45, back);

        ItemStack nextPage = new ItemStack(Material.GHAST_TEAR);
        ItemMeta nextPageMeta = nextPage.getItemMeta();
        nextPageMeta.setDisplayName(ConfigManager.getString("saved-gui.next-page"));
        nextPage.setItemMeta(nextPageMeta);

        inventory.setItem(53, nextPage);

        ItemStack prevPage = new ItemStack(Material.GOLD_NUGGET);
        ItemMeta prevMeta = prevPage.getItemMeta();
        prevMeta.setDisplayName(ConfigManager.getString("saved-gui.prev-page"));
        prevPage.setItemMeta(prevMeta);

        inventory.setItem(52, prevPage);

        for (int i = 0; i < 45 && i + page * 45 < itemStacks.size(); i++) {
            ItemStack itemStack = itemStacks.get(i + page * 45).clone();
            ItemMeta meta = itemStack.getItemMeta();
            List<String> lore = meta.getLore();
            if (lore == null) {
                lore = new ArrayList<>();
            }
            lore.add(ConfigManager.getString("saved-gui.tooltip-get"));
            lore.add(ConfigManager.getString("saved-gui.tooltip-remove"));
            meta.setLore(lore);
            itemStack.setItemMeta(meta);
            inventory.setItem(i, itemStack);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if (event.getRawSlot() == 45) {
            dispose(true);
        } else if (event.getRawSlot() + page * 45 < itemStacks.size() && event.getRawSlot() < 45) {
            if (event.isLeftClick()) {
                player.getInventory().addItem(itemStacks.get(event.getRawSlot() + page * 45));
                player.sendMessage(ConfigManager.getString("saved-gui.item-get"));
            } else if (event.isRightClick()) {
                int index = event.getRawSlot() + page * 45;
                Main.main.getItemsConfig().set(itemKeys.get(index), null);
                Main.main.saveItemConfig();
                itemKeys.remove(index);
                itemStacks.remove(index);
                createInventory(true);
            }
        } else if (event.getRawSlot() == 52) {
            if (page > 0) {
                --page;
                createInventory(true);
            }
        } else if (event.getRawSlot() == 53) {
            if (page < (itemStacks.size() - 1) / 45) {
                ++page;
                createInventory(true);
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory().equals(inventory)) {
            dispose(true);
        }
    }

    private void dispose(boolean resume) {
        HandlerList.unregisterAll(this);
        if (resume) {
            if (resumable != null) {
                resumable.resume();
            } else {
                player.closeInventory();
            }
        }
    }

}
