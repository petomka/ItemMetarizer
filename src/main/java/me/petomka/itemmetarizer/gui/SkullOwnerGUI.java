package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

/**
 * Created by Benedikt on 01.09.2017.
 */
public class SkullOwnerGUI implements Listener, Resumable {

    private Player player;
    private Inventory inventory;
    private Resumable resumable;
    private ItemStack toEdit;

    public SkullOwnerGUI(Player player, Resumable resumable, ItemStack toEdit) {
        this.player = player;
        this.resumable = resumable;
        this.toEdit = toEdit;
        if (!(toEdit.getItemMeta() instanceof SkullMeta)) {
            return;
        }
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void createInventory() {
        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("skull-gui.title"));

        ItemStack blackPane = MainGUI.getBlackGlassPane();

        for (int i = 0; i < 45; i++) {
            inventory.setItem(i, blackPane);
        }

        ItemStack bluePane = MainGUI.getBlueGlassPane();

        for (int i = 45; i < 54; i++) {
            inventory.setItem(i, bluePane);
        }

        ItemStack back = new ItemStack(Material.IRON_DOOR);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName(ConfigManager.getString("skull-gui.back"));
        back.setItemMeta(backMeta);

        inventory.setItem(45, back);

        ItemStack skull = new ItemStack(Material.SKULL_ITEM);
        ItemMeta skullItemMeta = skull.getItemMeta();

        for (short i = 0; i < 3; i++) {
            for(short j = 0;  j < 2; j++) {
                skullItemMeta.setDisplayName(ConfigManager.getString("skull-gui.change-type"));
                skull.setItemMeta(skullItemMeta);
                skull.setDurability((short) (j*3+i));
                inventory.setItem(9*(j+1) + 3 + i, skull);
            }
        }

        ItemStack setName = new ItemStack(Material.SIGN);
        ItemMeta setNameMeta = setName.getItemMeta();
        setNameMeta.setDisplayName(ConfigManager.getString("skull-gui.set-name"));
        setName.setItemMeta(setNameMeta);

        inventory.setItem(31, setName);

    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        switch (event.getRawSlot()) {
            case 45:
                dispose();
                break;
            case 12:
            case 13:
            case 14:
            case 21:
            case 22:
            case 23:
                changeSkullType(event.getRawSlot());
                break;
            case 31:
                setSkullOwner();
                break;
        }
    }

    private void changeSkullType(int rawSlot) {
        toEdit.setDurability((short) (((rawSlot/9)-1)*3+Math.abs(3-rawSlot%9)));
    }

    private void setSkullOwner() {
        if (toEdit.getDurability() == SkullType.PLAYER.ordinal()) {
            new ChatConfigurator(player, ConfigManager.getString("skull-gui.edit-skull-owner"), this, ChatConfigurator.VALUE_TYPE.STRING);
        } else {
            player.sendMessage(ConfigManager.getString("skull-gui.no-player"));
        }
    }

    private void dispose() {
        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
            resumable.resume();
            return null;
        });
    }

    @SuppressWarnings("deprecation")
    @Override
    public void resume(Object... params) {
        if (params.length > 1) {
            if (params[1] instanceof String) {
                updateItemStack();
                SkullMeta meta = (SkullMeta) toEdit.getItemMeta();
                meta.setOwner((String) params[1]);
                toEdit.setItemMeta(meta);
                player.getInventory().setItemInMainHand(toEdit);
            }
        }
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    @Override
    public void updateItemStack() {
        toEdit = player.getInventory().getItemInMainHand();
    }
}
