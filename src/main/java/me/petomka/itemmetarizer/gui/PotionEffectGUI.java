package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;

public class PotionEffectGUI implements Listener, Resumable {

    private final Player player;
    private final Resumable resumable;
    private Inventory inventory;
    private ItemStack toEdit;

    public PotionEffectGUI(Player player, ItemStack itemStack, Resumable resumable) {
        this.player = player;
        this.toEdit = itemStack;
        this.resumable = resumable;
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void createInventory() {
        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("potion-gui.title"));

        ItemStack potion = new ItemStack(Material.POTION, 1, (byte)0);
        PotionMeta potionMeta = (PotionMeta) potion.getItemMeta();

        double colorPi = 0;

        ArrayList<String> lore = new ArrayList<>();

        lore.add(ConfigManager.getString("potion-gui.left-click-add"));
        lore.add(ConfigManager.getString("potion-gui.right-click-remove"));

        for (int i = 1; i < PotionEffectType.values().length; i++) {
            int red = (int) (Math.sin(colorPi)*127 + 127);
            int green = (int) (Math.sin(colorPi + 2)*127 + 127);
            int blue = (int) (Math.sin(colorPi + 4)*127 + 127);
            potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.values()[i], 20 * 60, 0, false, true), true);
            colorPi += Math.PI/9;
            potionMeta.setColor(Color.fromRGB(red, green, blue));
            potionMeta.setDisplayName("§r" + makeSenseOutOfEnum(PotionEffectType.values()[i].getName()));
            potionMeta.setLore(lore);
            potion.setItemMeta(potionMeta);
            inventory.setItem(i - 1, potion);
            potionMeta.clearCustomEffects();
        }

        ItemStack back = new ItemStack(Material.IRON_DOOR);
        ItemMeta backMeta = back.getItemMeta();
        backMeta.setDisplayName(ConfigManager.getString("potion-gui.back-to-main"));
        back.setItemMeta(backMeta);

        inventory.setItem(45, back);

        ItemStack blackPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
        ItemMeta meta = blackPane.getItemMeta();
        meta.setDisplayName("§r");
        blackPane.setItemMeta(meta);

        for (int i = 46; i < 54; i++) {
            inventory.setItem(i, blackPane);
        }

        ItemStack setColor = new ItemStack(Material.INK_SACK, 1, (byte)4);
        ItemMeta colorMeta = setColor.getItemMeta();
        colorMeta.setDisplayName(ConfigManager.getString("potion-gui.set-color"));
        setColor.setItemMeta(colorMeta);

        inventory.setItem(49, setColor);
    }

    private String makeSenseOutOfEnum(String enumName) {
        String[] parts = enumName.split("_");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.length; i++) {
            String s = parts[i].toLowerCase();
            char f = s.charAt(0);
            f = Character.toUpperCase(f);
            s = f + s.substring(1, s.length());
            sb.append(s);
            if (i + 1 < parts.length) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if (event.getRawSlot() == 45) {
            dispose(true);
            return;
        }
        if(event.getRawSlot() == 49) {
            dispose(false);
            new ColorGUI(toEdit, player, this);
            return;
        }
        if (event.getRawSlot() + 1 < PotionEffectType.values().length) {
            if (event.isLeftClick()) {
                dispose(false);
                new PotionEffectTypeGUI(player, this, PotionEffectType.values()[event.getRawSlot() + 1], toEdit);
            } else if (event.isRightClick()) {
                PotionMeta meta = (PotionMeta) toEdit.getItemMeta();
                meta.removeCustomEffect(PotionEffectType.values()[event.getRawSlot()+1]);
                toEdit.setItemMeta(meta);
                player.getInventory().setItemInMainHand(toEdit);
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (event.getInventory().equals(inventory)) {
            dispose(true);
        }
    }

    private void dispose(boolean resume) {
        HandlerList.unregisterAll(this);
        if (resume) {
            Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
                resumable.resume();
                return null;
            });
        }
    }

    @Override
    public void resume(Object... params) {
        if (params.length > 0) {
            if (params[0] instanceof ItemStack) {
                toEdit = (ItemStack) params[0];
            }
        }
        updateItemStack();
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    @Override
    public void updateItemStack() {
        toEdit = player.getInventory().getItemInMainHand();
    }


}
