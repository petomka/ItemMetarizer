package me.petomka.itemmetarizer.gui;

import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.attributeapi.Attribute;
import me.petomka.itemmetarizer.attributeapi.AttributeAPI;
import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Benedikt on 28.08.2017.
 */
public class AttributesGUI implements Listener, Resumable {

    private ItemStack toEdit;
    private Player player;
    private Resumable resumable;
    private Inventory inventory;
    private AttributeAPI attributeAPI;

    public AttributesGUI(ItemStack toEdit, Player player, Resumable resumable) {
        this.toEdit = toEdit;
        this.player = player;
        this.resumable = resumable;
        attributeAPI = new AttributeAPI(toEdit);
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    enum PropertyIcon {
        health(Material.GOLDEN_APPLE, 1, "health", 11, Attribute.MAX_HEALTH),
        //followRange(Material.COMPASS, 0, "follow-range", 12, Attribute.FOLLOW_RANGE),
        knockRes(Material.IRON_BLOCK, 0, "knockback-resistance", 12, Attribute.KNOCKBACK_RESISTANCE),
        moveSpeed(Material.SUGAR, 0, "movement-speed", 13, Attribute.MOVEMENT_SPEED),
        attackDmg(Material.IRON_AXE, 0, "attack-damage", 14, Attribute.ATTACK_DAMAGE),
        armor(Material.IRON_CHESTPLATE, 0, "armor", 15, Attribute.ARMOR),
        armorToughness(Material.DIAMOND_CHESTPLATE, 0, "armor-toughness", 20, Attribute.ARMOR_THOUGHNESS),
        attackSpeed(Material.DIAMOND_SWORD, 0, "attack-speed", 21, Attribute.ATTACK_SPEED),
        luck(Material.GOLD_NUGGET, 0, "luck", 22, Attribute.LUCK),
        //jump(Material.GOLD_BARDING, 0, "horse-jump-strength", 24, Attribute.JUMP_STRENGTH),
        //spawnReinforce(Material.SKULL_ITEM, 2, "zombie-spawn-reinforcements", 30, Attribute.SPAWN_REINFORCEMENTS),
        canBreak(Material.IRON_PICKAXE, 0, "can-break", 23, Attribute.CAN_DESTROY),
        canPlaceOn(Material.LOG, 0, "can-place-on", 24, Attribute.CAN_PLACED_ON);

        ItemStack icon;
        int slot;
        private Attribute attribute;

        PropertyIcon(Material material, int damage, String displayNamePath, int slot, Attribute attribute) {
            icon = new ItemStack(material);
            icon.setDurability((short)damage);
            ItemMeta iconMeta = icon.getItemMeta();
            iconMeta.setDisplayName(ConfigManager.getString("attributes-gui.edit-" + displayNamePath));
            icon.setItemMeta(iconMeta);
            this.slot = slot;
            this.attribute = attribute;
        }
    }

    private void createInventory() {
        inventory = Bukkit.createInventory(player, 54, ConfigManager.getString("attributes-gui.title"));

        ItemStack blackPane = MainGUI.getBlackGlassPane();

        for(int i = 0; i < 45; i++) {
            inventory.setItem(i, blackPane);
        }

        ItemStack bluePane = MainGUI.getBlueGlassPane();

        for(int i = 45; i < 54; i++) {
            inventory.setItem(i, bluePane);
        }

        ItemStack exit = new ItemStack(Material.IRON_DOOR);
        ItemMeta exitMeta = exit.getItemMeta();
        exitMeta.setDisplayName(ConfigManager.getString("attributes-gui.exit"));
        exit.setItemMeta(exitMeta);
        inventory.setItem(45, exit);

        for(PropertyIcon icon : PropertyIcon.values()) {
            inventory.setItem(icon.slot, icon.icon);
        }
    }

    public void resume(Object... params) {
        updateItemStack();
        createInventory();
        player.openInventory(inventory);
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    public void updateItemStack() {
        toEdit = player.getInventory().getItemInMainHand();
    }

    private void dispose(boolean resume) {
        HandlerList.unregisterAll(this);
        if(resume) {
            Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
                resumable.resume();
                return null;
            });
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        event.setCancelled(true);
        if(event.getRawSlot() == 45) {
            dispose(true);
            return;
        }
        for(PropertyIcon icon : PropertyIcon.values()) {
            if(event.getRawSlot() == icon.slot) {
                dispose(false);
                new AttributeGenericModifyGUI(player, toEdit, this, icon.attribute, attributeAPI, icon.icon);
                return;
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if(!event.getInventory().equals(inventory)) {
            return;
        }
        dispose(true);
    }
}
