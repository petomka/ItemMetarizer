package me.petomka.itemmetarizer.gui;

import com.google.common.collect.Maps;
import me.petomka.itemmetarizer.Main;
import me.petomka.itemmetarizer.config.ConfigManager;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;

import java.util.Map;

/**
 * Created by Benedikt on 24.08.2017.
 */
public class ChatConfigurator implements Listener {

	private static Map<Player, ChatConfigurator> currentConfigurators = Maps.newHashMap();

	public static boolean answerChatConfigurator(Player player, String text) {
		if(!currentConfigurators.containsKey(player)) {
			return false;
		}
		currentConfigurators.get(player).dispose(text);
		return true;
	}

    private Player player;
    
    private Resumable resumable;

    private String property;

    public enum VALUE_TYPE {
        STRING,
        DOUBLE
    }

    public ChatConfigurator(Player player, MainGUI.Property property, Resumable resumable, VALUE_TYPE value_type) {
        this(player, property.name(), resumable, value_type);
    }

    public ChatConfigurator(Player player, String property, Resumable resumable, VALUE_TYPE value_type) {
    	currentConfigurators.computeIfPresent(player, (player1, chatConfigurator) -> {
    		chatConfigurator.dispose("");
    		return null; //entfernt aus Map
		});
    	currentConfigurators.put(player, this);
        this.player = player;
        player.closeInventory();
        this.property = property;
        this.resumable = resumable;
        player.sendMessage(ConfigManager.getString("chat-config.setup").replace("<property>", property));
        switch (value_type) {
            case STRING:
                player.sendMessage(ConfigManager.getString("chat-config.enter-string"));
                break;
            case DOUBLE:
                player.sendMessage(ConfigManager.getString("chat-config.enter-double"));
                break;
        }
        Bukkit.getPluginManager().registerEvents(this, Main.main);
    }

    private void dispose(String string) {
        HandlerList.unregisterAll(this);
        Bukkit.getScheduler().callSyncMethod(Main.main, () -> {
            resumable.resume(property, ChatColor.translateAlternateColorCodes('&', string));
            return null;
        });
        currentConfigurators.remove(player);
    }

    @EventHandler
    public void onSlotChange(PlayerItemHeldEvent event) {
        if(event.getPlayer().equals(player)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onChat(AsyncPlayerChatEvent event) {
        if(!event.getPlayer().equals(player)) {
            return;
        }
        event.setCancelled(true);
        dispose(event.getMessage());
        event.setMessage("");
    }
}
