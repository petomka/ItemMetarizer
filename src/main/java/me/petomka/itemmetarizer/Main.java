package me.petomka.itemmetarizer;

import me.petomka.itemmetarizer.commands.CommandItemmeta;
import me.petomka.itemmetarizer.commands.CommandManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Benedikt on 23.08.2017.
 */
public class Main extends JavaPlugin {

    public static Main main;

    public static FileConfiguration config;

    @Override
    public void onEnable() {
        main = this;
        initConfig();
        initCommands();
        initSaveFile();
        getLogger().info("ItemMetarizer has been enabled.");
    }

    @Override
    public void onDisable() {
        getLogger().info("ItemMetarizer has been disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        CommandManager.onCommand(sender, command, label, args);
        return true;
    }

    private void initConfig() {
        config = getConfig();

        config.addDefault("prefix", "&6&lIM &8&l> &r");
        config.addDefault("no-permission", "<prefix>&cInsufficient permissions.");
        config.addDefault("from-console", "You can only use this from in-game.");
        config.addDefault("usage-format", "<prefix>&4Usage: &c<usage>");
        config.addDefault("allow-oversized-stacks", false);

        config.addDefault("command.no-item", "<prefix>&cYou must be holding an item to edit.");
        config.addDefault("command.help.header", "&8&m--------------&8&l< &6&lItemMetarizer Help &8&l>&8&m--------------");
        config.addDefault("command.help.format", "&e/<command>&8: &7<description>");
        config.addDefault("command.help.footer", "&8&m------------------&8&l< &6&lPage <page>/<maxpage> &8&l>&8&m------------------");

        config.addDefault("chat-config.setup", "<prefix>&6You are editing the following property: &e<property>");
        config.addDefault("chat-config.enter-string", "&6Please enter a string.");
        config.addDefault("chat-config.enter-double", "&6Please enter a floating point number (e.g. '1.0' or '0.5').");
        config.addDefault("chat-config.not-in-config", "<prefix>&cYou are currently not in conversation with ItemMetarizer.");

        config.addDefault("main-gui.title", "&2Change Item");

        config.addDefault("main-gui.edit-name", "&aChange Name");
        config.addDefault("main-gui.edit-lore", "&aChange Lore");
        config.addDefault("main-gui.edit-enchants", "&2Change Enchantments");
        config.addDefault("main-gui.edit-unbreakable", "&2Toggle Unbreakable");
        config.addDefault("main-gui.edit-attributes", "&eChange Attributes");
        config.addDefault("main-gui.edit-durability", "&eChange Durability");
        config.addDefault("main-gui.edit-flags", "&cChange Itemflags");
        //config.addDefault("main-gui.edit-reset", "&4Reset this Item");
        config.addDefault("main-gui.edit-amount", "&cSet amount");
        config.addDefault("main-gui.edit-save", "&6Save this item");
        config.addDefault("main-gui.edit-saved_list", "&6Show saved items");
        config.addDefault("main-gui.item-saved", "<prefix>&aItem was successfully saved.");

        config.addDefault("main-gui.edit-special", "&dEdit special properties");
        config.addDefault("main-gui.no-special", "&cThis Item has no special property to edit.");

        config.addDefault("saved-gui.title", "&0Saved Items - &6Page <page>");
        config.addDefault("saved-gui.back", "&cBack");
        config.addDefault("saved-gui.next-page", "&6Next page");
        config.addDefault("saved-gui.prev-page", "&6Previous page");
        config.addDefault("saved-gui.item-get", "<prefix>&aItem successfully added to your inventory.");
        config.addDefault("saved-gui.tooltip-get", "&a&lLMB> &aGet this item");
        config.addDefault("saved-gui.tooltip-remove", "&c&lRMB> &cRemove this item");

        config.addDefault("lore-gui.title", "&2Edit Lore");
        config.addDefault("lore-gui.line-pattern", "&7&lLine &f<line>");
        config.addDefault("lore-gui.move-line", "&aLeft click to move this line");
        config.addDefault("lore-gui.edit-line", "&eMiddle click to edit this line");
        config.addDefault("lore-gui.delete-line", "&cRight click to delete this line");
        config.addDefault("lore-gui.add-line", "&2Add line");
        config.addDefault("lore-gui.delete-all", "&4Delete all lines");
        config.addDefault("lore-gui.back-to-menu", "&cExit to main menu");

        config.addDefault("enchant-gui.title", "&2Edit Enchantments");
        config.addDefault("enchant-gui.exit-to-menu", "&cBack to main menu");
        config.addDefault("enchant-gui.next-page", "&6Next page");
        config.addDefault("enchant-gui.prev-page", "&6Previous page");
        config.addDefault("enchant-gui.edit-enchant", "&aLeft click to edit enchantment level");
        config.addDefault("enchant-gui.remove-enchant", "&cRight click to remove enchantment");
        config.addDefault("enchant-gui.add-group", "&aLeft click to add enchant group");
        config.addDefault("enchant-gui.remove-group", "&cRight click to remove enchant group");
        config.addDefault("enchant-gui.add-all", "&aAdd all enchantments");
        config.addDefault("enchant-gui.remove-all", "&cRemove all enchantments");
        config.addDefault("enchant-gui.armor", "&bArmor Enchantments");
        config.addDefault("enchant-gui.weapon", "&bWeapon Enchantments");
        config.addDefault("enchant-gui.tool", "&bTool Enchantments");
        config.addDefault("enchant-gui.bow", "&bBow Enchantments");
        config.addDefault("enchant-gui.fishing_rod", "&bFishing rod Enchantments");
        config.addDefault("enchant-gui.universal", "&bUniversal Enchantments");
        config.addDefault("enchant-gui.curse", "&cCurses");
        config.addDefault("enchant-gui.modify-amount", "&eChange value by <amount>");
        config.addDefault("enchant-gui.modify-decrease", "&cRight click to decrease &7/ &4Shift right click to decrease x10");
        config.addDefault("enchant-gui.modify-increase", "&aLeft click to increase &7/ &2Shift left click to increase x10");

        config.addDefault("damage-gui.title", "&2Edit Itemdamage");
        config.addDefault("damage-gui.exit-to-menu", "&cBack");
        config.addDefault("damage-gui.edit-value", "&3Edit value by hand");
        config.addDefault("damage-gui.modify-amount", "&eChange value by <amount>");
        config.addDefault("damage-gui.modify-increase", "&bLeft click to increase &7/ &3Shift left click to increase x10");
        config.addDefault("damage-gui.modify-decrease", "&eRight click to decrease &7/ &6Shift right click to decrease x10");

        config.addDefault("flags-gui.title", "&2Edit Itemflags");
        config.addDefault("flags-gui.hidden", "&7This property is hidden");
        config.addDefault("flags-gui.shown", "&aThis property is shown");
        config.addDefault("flags-gui.hide-attributes", "&6Display Attributes");
        config.addDefault("flags-gui.hide-enchants", "&6Display Enchantments");
        config.addDefault("flags-gui.hide-potions", "&6Display Potion Effects");
        config.addDefault("flags-gui.hide-destroys", "&6Display \"&eCan destroy&6\"");
        config.addDefault("flags-gui.hide-placed-on", "&6Dislay \"&eCan be placed on&6\"");
        config.addDefault("flags-gui.hide-unbreakable", "&6Display \"&9Unbreakable&6\"");
        config.addDefault("flags-gui.exit-to-menu", "&cBack to main menu");

        config.addDefault("attributes-gui.title", "&2Edit Attributes");
        config.addDefault("attributes-gui.exit", "&cBack to menu");
        config.addDefault("attributes-gui.edit-health", "&2Edit Health Boost");
        config.addDefault("attributes-gui.edit-follow-range", "&5Edit Follow Range (Mobs)");
        config.addDefault("attributes-gui.edit-knockback-resistance", "&bEdit Knockback Resistance");
        config.addDefault("attributes-gui.edit-movement-speed", "&bEdit Movement Speed");
        config.addDefault("attributes-gui.edit-attack-damage", "&cEdit Attack Damage");
        config.addDefault("attributes-gui.edit-armor", "&eEdit Armor");
        config.addDefault("attributes-gui.edit-armor-toughness", "&eEdit Armour Toughness");
        config.addDefault("attributes-gui.edit-attack-speed", "&cEdit Attack Speed");
        config.addDefault("attributes-gui.edit-luck", "&6Edit Luck");
        config.addDefault("attributes-gui.edit-horse-jump-strength", "&6Edit Horse Jump Strength");
        config.addDefault("attributes-gui.edit-zombie-spawn-reinforcements", "&4Edit Spawn Zombie Reinforcements (Zombie)");
        config.addDefault("attributes-gui.edit-can-break", "&3Edit Can Break");
        config.addDefault("attributes-gui.edit-can-place-on", "&3Edit Can Place On");
        config.addDefault("attributes-gui.add-flat", "&bAdd Amount as Number");
        config.addDefault("attributes-gui.add-percentage", "&9Add Amount as Scalar");
        config.addDefault("attributes-gui.multiply-percentage", "&3Multiply Amount as Scalar");
        config.addDefault("attributes-gui.left-click-set-slot", "&7&oLeft click to set this Slot active");
        config.addDefault("attributes-gui.active-slot", "&cThis Slot is currently active!");
        config.addDefault("attributes-gui.slot-mainhand", "&bMain Hand");
        config.addDefault("attributes-gui.slot-offhand", "&bOff Hand");
        config.addDefault("attributes-gui.slot-helmet", "&bHelmet");
        config.addDefault("attributes-gui.slot-breast", "&bChest");
        config.addDefault("attributes-gui.slot-legs", "&bLeggings");
        config.addDefault("attributes-gui.slot-feet", "&bFeet");
        config.addDefault("attributes-gui.set-amount", "&bSet Amount");
        config.addDefault("attributes-gui.specify-amount-first", "<prefix>&cPlease specify an amount before editing slot or operator.");

        config.addDefault("attributes-gui.break-place.back", "&cBack to attribute menu");
        config.addDefault("attributes-gui.break-place.next-page", "&6Next page");
        config.addDefault("attributes-gui.break-place.prev-page", "&6Previous page");

        config.addDefault("amount-gui.title", "&cSet amount");
        config.addDefault("amount-gui.add", "&a&lLMB>&a + <amount>");
        config.addDefault("amount-gui.subtract", "&c&lRMB>&c - <amount>");
        config.addDefault("amount-gui.back", "&cBack to main menu");

        config.addDefault("potion-gui.title", "&bChange Potion Effects");
        config.addDefault("potion-gui.back-to-main", "&cBack to main menu");
        config.addDefault("potion-gui.back-to-menu", "&cBack to potion menu");
        config.addDefault("potion-gui.left-click-add", "&aLeft click to add/change this effect");
        config.addDefault("potion-gui.right-click-remove", "&cRight click to remove this effect");
        config.addDefault("potion-gui.set-color", "&b&lSet color");
        config.addDefault("potion-gui.duration-display-name", "&b&lDuration");
        config.addDefault("potion-gui.time-in-ticks", "&7<time> ticks");
        config.addDefault("potion-gui.time-in-minutes", "&7<time> minutes");
        config.addDefault("potion-gui.amplifier-display-name", "&b&lAmplifier");
        config.addDefault("potion-gui.amplifier-display-amount", "&7<amplifier>");
        config.addDefault("potion-gui.amplifier-modify-amount-add", "&a&l> &a+ <amount>");
        config.addDefault("potion-gui.amplifier-modify-amount-decrease", "&c- <amount> &c&l<");
        config.addDefault("potion-gui.time-modify-amount-add", "&a&l> &a+ <time_ticks> ticks (<time> minutes)");
        config.addDefault("potion-gui.time-modify-amount-shift-add", "&2&lSHIFT> &2+ <time_ticks> ticks (<time> minutes)");
        config.addDefault("potion-gui.time-modify-amount-decrease", "&c- <time_ticks> ticks (<time> minutes) &c&l<");
        config.addDefault("potion-gui.time-modify-amount-shift-decrease", "&4- <time_ticks> ticks (<time> minutes) &4&l<SHIFT");
        config.addDefault("potion-gui.particle-switch", "&bToggle particles");
        config.addDefault("potion-gui.particles-true", "&aEnabled");
        config.addDefault("potion-gui.particles-false", "&7Disabled");

        config.addDefault("color-gui.title", "&fAdjust &4&lR&2&lG&1&lB");
        config.addDefault("color-gui.red-display", "&4&lRed");
        config.addDefault("color-gui.add-red", "&c+ <amount>");
        config.addDefault("color-gui.subtract-red", "&c- <amount>");
        config.addDefault("color-gui.green-display", "&2&lGreen");
        config.addDefault("color-gui.add-green", "&a+ <amount>");
        config.addDefault("color-gui.subtract-green", "&a- <amount>");
        config.addDefault("color-gui.blue-display", "&2&lBlue");
        config.addDefault("color-gui.add-blue", "&9+ <amount>");
        config.addDefault("color-gui.subtract-blue", "&9- <amount>");
        config.addDefault("color-gui.back", "&cBack");
        config.addDefault("color-gui.color-display", Arrays.asList("&cRed: &7<red>", "&aGreen: &7<green>", "&9Blue: &7<blue>", "&8Hex: &7#<hex>"));

        config.addDefault("skull-gui.title", "&2Edit Skull");
        config.addDefault("skull-gui.change-type", "&6Change Skull Type");
        config.addDefault("skull-gui.back", "&cBack");
        config.addDefault("skull-gui.set-name", "&bSet skull owner");
        config.addDefault("skull-gui.edit-skull-owner", "Skull owner");
        config.addDefault("skull-gui.no-player", "<prefix>&cThat skull cannot have an owner.");

        config.options().copyDefaults(true);
        saveConfig();
    }

    private File itemsFile;
    private FileConfiguration itemsConfig;

    private void initSaveFile() {
        itemsFile = new File("plugins/ItemMetarizer/items.yml");
        if(!itemsFile.exists()) {
            try {
                itemsFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        itemsConfig = YamlConfiguration.loadConfiguration(itemsFile);
    }

    public FileConfiguration getItemsConfig() {
        return itemsConfig;
    }

    public void saveItemConfig() {
        try {
            itemsConfig.save(itemsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initCommands() {
        new CommandItemmeta();
    }

}
