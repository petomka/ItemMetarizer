package me.petomka.itemmetarizer.nms;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.lang.reflect.Method;

@UtilityClass
public class CBUtils {

	public static final Class<?> craftItemStack = ReflectionUtils.getCraftBukkitClass("inventory.CraftItemStack");

	private static Method asNMSCopy;
	private static Method asBukkitCopy;
	private static Method asNewCraftStack;
	private static Method nmsStackHasTag;

	static {
		try {
			asNMSCopy = craftItemStack.getMethod("asNMSCopy", ItemStack.class);
			asBukkitCopy = craftItemStack.getMethod("asBukkitCopy", NmsUtils.getItemStack());
			asNewCraftStack = craftItemStack.getMethod("asNewCraftStack", NmsUtils.getItem());
			nmsStackHasTag = craftItemStack.getMethod("hasTag", ReflectionUtils.NO_PARAMETERS);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@SneakyThrows
	public static @Nullable Object asNMSCopy(ItemStack itemStack) {
		return asNMSCopy.invoke(null, craftItemStack);
	}

	@SneakyThrows
	public static ItemStack asBukkitCopy(Object nmsStack) {
		return (ItemStack) asBukkitCopy.invoke(null, nmsStack);
	}

	@SneakyThrows
	public static ItemStack asNewCraftStack(Object nmsItem) {
		return (ItemStack) asNewCraftStack.invoke(null, nmsItem);
	}

	@SneakyThrows
	public static boolean hasTag(Object nmsStack) {
		return (boolean) nmsStackHasTag.invoke(nmsStack, ReflectionUtils.NO_ARGUMENTS);
	}

}
