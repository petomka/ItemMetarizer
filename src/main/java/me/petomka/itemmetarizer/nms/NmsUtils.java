package me.petomka.itemmetarizer.nms;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Set;

@UtilityClass
public class NmsUtils {

	private static Class<?> block = ReflectionUtils.getNmsClass("Block");
	@Getter
	private static Class<?> item = ReflectionUtils.getNmsClass("Item");
	@Getter
	private static Class<?> itemStack = ReflectionUtils.getNmsClass("ItemStack");
	private static Class<?> minecraftKey = ReflectionUtils.getNmsClass("MinecraftKey");
	private static Class<?> nbtBase = ReflectionUtils.getNmsClass("NBTBase");
	@Getter
	private static Class<?> nbtTagCompound = ReflectionUtils.getNmsClass("NBTTagCompound");
	private static Class<?> nbtTagList = ReflectionUtils.getNmsClass("NBTTagList");
	private static Class<?> nbtTagString = ReflectionUtils.getNmsClass("NBTTagString");
	private static Class<?> iRegistry = ReflectionUtils.getNmsClass("IRegistry");

	private static Constructor<?> nbtTagCompoundConstructor;
	private static Constructor<?> nbtTagListConstructor;
	private static Constructor<?> nbtTagStringConstructor;
	private static Constructor<?> minecraftKeyConstructor;

	private static Method nmsStackHasTag;
	private static Method nmsStackGetTag;
	private static Method nmsStackGetOrCreateTag;
	private static Method nmsStackGetItem;

	private static Method itemGetOf;

	private static Method nbtTagCompoundGetKeys;
	private static Method nbtTagCompoundHasKey;
	private static Method nbtTagCompoundGet;
	private static Method nbtTagCompoundGetString;
	private static Method nbtTagCompoundGetInt;
	private static Method nbtTagCompoundGetLong;
	private static Method nbtTagCompoundGetDouble;

	private static Method nbtTagCompoundSet;
	private static Method nbtTagCompoundSetString;
	private static Method nbtTagCompoundSetInt;
	private static Method nbtTagCompoundSetLong;
	private static Method nbtTagCompoundSetShort;
	private static Method nbtTagCompoundSetDouble;

	private static Method nbtTagListAdd;
	private static Method nbtTagListSize;
	private static Method nbtTagListGet;
	private static Method nbtTagListGetString;
	private static Method nbtTagListRemove;

	private static Method blockAsBlock;

	static {
		try {
			nbtTagCompoundConstructor = nbtTagCompound.getConstructor(ReflectionUtils.NO_PARAMETERS);
			nbtTagListConstructor = nbtTagList.getConstructor(ReflectionUtils.NO_PARAMETERS);
			nbtTagStringConstructor = nbtTagString.getConstructor(String.class);
			minecraftKeyConstructor = minecraftKey.getConstructor(String.class);

			nmsStackHasTag = itemStack.getMethod("hasTag", ReflectionUtils.NO_PARAMETERS);
			nmsStackGetTag = itemStack.getMethod("getTag", ReflectionUtils.NO_PARAMETERS);
			nmsStackGetOrCreateTag = itemStack.getMethod("getOrCreateTag", ReflectionUtils.NO_PARAMETERS);
			nmsStackGetItem = itemStack.getMethod("getItem", ReflectionUtils.NO_PARAMETERS);

			itemGetOf = item.getMethod("getItemOf", block);

			nbtTagCompoundGetKeys = nbtTagCompound.getMethod("getKeys", ReflectionUtils.NO_PARAMETERS);
			nbtTagCompoundHasKey = nbtTagCompound.getMethod("hasKey", String.class);
			nbtTagCompoundGet = nbtTagCompound.getMethod("get", String.class);
			nbtTagCompoundGetString = nbtTagCompound.getMethod("getString", String.class);
			nbtTagCompoundGetInt = nbtTagCompound.getMethod("getInt", String.class);
			nbtTagCompoundGetLong = nbtTagCompound.getMethod("getLong", String.class);
			nbtTagCompoundGetDouble = nbtTagCompound.getMethod("getDouble", String.class);
			nbtTagCompoundSet = nbtTagCompound.getMethod("set", String.class, nbtBase);
			nbtTagCompoundSetString = nbtTagCompound.getMethod("setString", String.class, String.class);
			nbtTagCompoundSetInt = nbtTagCompound.getMethod("setInt", String.class, int.class);
			nbtTagCompoundSetLong = nbtTagCompound.getMethod("setlong", String.class, long.class);
			nbtTagCompoundSetShort = nbtTagCompound.getMethod("setshort", String.class, short.class);
			nbtTagCompoundSetDouble = nbtTagCompound.getMethod("setDouble", String.class, double.class);
			nbtTagListAdd = nbtTagList.getMethod("add", Object.class);
			nbtTagListSize = nbtTagList.getMethod("size", ReflectionUtils.NO_PARAMETERS);
			nbtTagListGet = nbtTagList.getMethod("get", int.class);
			nbtTagListGetString = nbtTagList.getMethod("getString", int.class);
			nbtTagListRemove = nbtTagList.getMethod("remove", int.class);

			blockAsBlock = block.getMethod("asBlock", itemStack);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@SneakyThrows
	public static boolean nmsStackHasTag(Object nmsStack) {
		return (boolean) nmsStackHasTag.invoke(nmsStack, ReflectionUtils.NO_ARGUMENTS);
	}

	@SneakyThrows
	public static Object nmsStackGetTag(Object nmsStack) {
		return nmsStackGetTag.invoke(nmsStack, ReflectionUtils.NO_ARGUMENTS);
	}

	@SneakyThrows
	public static void nbtTagCompoundSet(Object nbtTagCompound, String key, Object nbtBase) {
		nbtTagCompoundSet.invoke(nbtTagCompound, key, nbtBase);
	}

	@SneakyThrows
	public static Object toNbtTagString(String value) {
		return nbtTagStringConstructor.newInstance(value);
	}

	@SneakyThrows
	public static Object nmsStackGetOrCreateTag(Object nmsStack) {
		return nmsStackGetOrCreateTag.invoke(nmsStack, ReflectionUtils.NO_ARGUMENTS);
	}

	@SneakyThrows
	public static Object nmsStackGetItem(Object nmsStack) {
		return nmsStackGetItem.invoke(nmsStack, ReflectionUtils.NO_ARGUMENTS);
	}

	@SneakyThrows
	public static Object getItemOf(Object block) {
		return itemGetOf.invoke(null, block);
	}

	@SneakyThrows
	public static void nbtTagCompoundSetString(Object nbtTagCompound, String key, String value) {
		nbtTagCompoundSetString.invoke(nbtTagCompound, key, value);
	}

	@SneakyThrows
	public static void nbtTagCompoundSetInt(Object nbtTagCompound, String key, int value) {
		nbtTagCompoundSetInt.invoke(nbtTagCompound, key, value);
	}

	@SneakyThrows
	public static void nbtTagCompoundSetLong(Object nbtTagCompound, String key, long value) {
		nbtTagCompoundSetLong.invoke(nbtTagCompound, key, value);
	}

	@SneakyThrows
	public static void nbtTagCompoundSetShort(Object nbtTagCompound, String key, short value) {
		nbtTagCompoundSetShort.invoke(nbtTagCompound, key, value);
	}

	@SneakyThrows
	public static void nbtTagCompoundSetDouble(Object nbtTagCompound, String key, double value) {
		nbtTagCompoundSetDouble.invoke(nbtTagCompound, key, value);
	}

	@SneakyThrows
	public static Object newNbtTagCompound() {
		return nbtTagCompoundConstructor.newInstance(ReflectionUtils.NO_ARGUMENTS);
	}

	@SneakyThrows
	public static Object newNbtTagList() {
		return nbtTagListConstructor.newInstance(ReflectionUtils.NO_ARGUMENTS);
	}

	@SneakyThrows
	public static Object newMinecraftKey(String value) {
		return minecraftKeyConstructor.newInstance(value);
	}

	@SneakyThrows
	public static void nbtTagListAdd(Object nbtTagList, Object nbtBase) {
		nbtTagListAdd.invoke(nbtTagList, nbtBase);
	}

	@SneakyThrows
	public static int nbtTagListSize(Object nbtTagList) {
		return (int) nbtTagListSize.invoke(nbtTagList, ReflectionUtils.NO_ARGUMENTS);
	}

	@SneakyThrows
	public static Object nbtTaglistGet(Object nbtTagList, int value) {
		return nbtTagListGet.invoke(nbtTagList, value);
	}

	@SneakyThrows
	public static String nbtTaglistGetString(Object nbtTagList, int value) {
		return (String) nbtTagListGetString.invoke(nbtTagList, value);
	}

	@SneakyThrows
	public static Object nbtTagListRemove(Object nbtTagList, int value) {
		return nbtTagListRemove.invoke(nbtTagList, value);
	}

	@SneakyThrows
	public static Set<String> nbtTagCompoundGetKeys(Object nbtTagCompound) {
		return (Set<String>) nbtTagCompoundGetKeys.invoke(nbtTagCompound);
	}

	@SneakyThrows
	public static boolean nbtTagCompoundHasKey(Object nbtTagCompound, String key) {
		return (boolean) nbtTagCompoundHasKey.invoke(nbtTagCompound, key);
	}

	@SneakyThrows
	public static Object nbtTagCompoundGet(Object nbtTagCompound, String key) {
		return nbtTagCompoundGet.invoke(nbtTagCompound, key);
	}

	@SneakyThrows
	public static String nbtTagCompoundGetString(Object nbtTagCompound, String key) {
		return (String) nbtTagCompoundGetString.invoke(nbtTagCompound, key);
	}

	@SneakyThrows
	public static int nbtTagCompoundGetInt(Object nbtTagCompound, String key) {
		return (int) nbtTagCompoundGetInt.invoke(nbtTagCompound, key);
	}

	@SneakyThrows
	public static long nbtTagCompoundGetLong(Object nbtTagCompound, String key) {
		return (long) nbtTagCompoundGetLong.invoke(nbtTagCompound, key);
	}

	@SneakyThrows
	public static double nbtTagCompoundGetDouble(Object nbtTagCompound, String key) {
		return (double) nbtTagCompoundGetDouble.invoke(nbtTagCompound, key);
	}

	@SneakyThrows
	public static Object getMinecraftKeyForBlock(Object block) {
		Object blocksRegistry = iRegistry.getDeclaredField("BLOCKS").get(null);
		return blocksRegistry.getClass().getMethod("getKey", Object.class).invoke(blocksRegistry, block);
	}

	@SneakyThrows
	public static Object getBlockForMinecraftKey(Object minecraftKey) {
		Object blocksRegistry = iRegistry.getDeclaredField("BLOCKS").get(null);
		return blocksRegistry.getClass().getMethod("getOrDefault", NmsUtils.minecraftKey).invoke(blocksRegistry, minecraftKey);
	}

	@SneakyThrows
	public static Object blockAsBlock(Object nmsItem) {
		return blockAsBlock.invoke(nmsItem);
	}

}
