package me.petomka.itemmetarizer.nms;

import lombok.experimental.UtilityClass;
import org.bukkit.Bukkit;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@UtilityClass
public class ReflectionUtils {

	public static final Class<?>[] NO_PARAMETERS = new Class[0];
	public static final Object[] NO_ARGUMENTS = new Object[0];

	private static final String VERSION;

	static {
		String path = Bukkit.getServer().getClass().getPackage().getName();
		VERSION = path.substring(path.lastIndexOf('.') + 1);
	}

	public static @Nullable Class<?> getNmsClass(@Nonnull String path) {
		return getClass("net.minecraft.server." + VERSION + path);
	}

	public static @Nullable Class<?> getCraftBukkitClass(@Nonnull String path) {
		return getClass("org.bukkit.craftbukkit." + VERSION + path);
	}

	public static @Nullable Class<?> getClass(@Nonnull String path) {
		try {
			return Class.forName(path);
		} catch (ClassNotFoundException e) {
			return null;
		}
	}

}
