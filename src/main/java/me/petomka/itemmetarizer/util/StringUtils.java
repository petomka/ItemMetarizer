package me.petomka.itemmetarizer.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringUtils {

	public static boolean isNullOrEmpty(String val) {
		return val == null || val.isEmpty();
	}

}
