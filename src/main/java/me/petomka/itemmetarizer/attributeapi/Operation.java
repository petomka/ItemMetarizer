package me.petomka.itemmetarizer.attributeapi;

/**
 * Created by Benedikt on 29.08.2017.
 */
public enum Operation {
    ADD_FLAT(0),
    ADD_PERCENTAGE(1),
    MULTIPLY_PERCENTAGE(2);
    Operation(int i) {
        this.i = i;
    }
    int i;

    public static Operation fromInt(int op) {
        switch (op) {
            default:
            case 0:
                return ADD_FLAT;
            case 1:
                return ADD_PERCENTAGE;
            case 2:
                return MULTIPLY_PERCENTAGE;
        }
    }
}
