package me.petomka.itemmetarizer.attributeapi;

import java.util.UUID;

/**
 * Created by Benedikt on 29.08.2017.
 */
public class AttributeInfo {

    public Attribute attribute;
    public Operation operation;
    public Slot slot;
    public double amount;
    public String name;
    public UUID uuid;

    public AttributeInfo(Attribute attribute, Operation operation, Slot slot, double amount, String name, UUID uuid) {
        this.attribute = attribute;
        this.operation = operation;
        this.slot = slot;
        this.amount = amount;
        this.name = name;
        this.uuid = uuid;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
