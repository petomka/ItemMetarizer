package me.petomka.itemmetarizer.attributeapi;

/**
 * Created by Benedikt on 29.08.2017.
 */
public enum Slot {
    MAIN_HAND ("mainhand"),
    OFF_HAND ("offhand"),
    FEET ("feet"),
    LEGS ("legs"),
    CHEST ("chest"),
    HEAD ("head");
    private String name;
    Slot(String name) {
        this.name = name;
    }
    /**
     * Get the predefined, global and unique name of this slot.
     *
     * @return The name
     */
    public String getName() {
        return this.name;
    }

    public static Slot byString(String key) {
        for(Slot slot : Slot.values()) {
            if(slot.getName().equalsIgnoreCase(key)) {
                return slot;
            }
        }
        return null;
    }
}
