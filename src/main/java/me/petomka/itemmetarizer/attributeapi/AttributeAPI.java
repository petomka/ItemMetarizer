package me.petomka.itemmetarizer.attributeapi;

import me.petomka.itemmetarizer.nms.CBUtils;
import me.petomka.itemmetarizer.nms.NmsUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Benedikt on 29.08.2017.
 */
public class AttributeAPI {

	private ItemStack itemStack;

	private Object modifiersNbtTagList;

	public AttributeAPI(ItemStack stack) {
		Object nmsStack = CBUtils.asNMSCopy(stack);

		if (CBUtils.hasTag(nmsStack)) {
			Object nbtTag = NmsUtils.nmsStackGetTag(nmsStack);
			modifiersNbtTagList = NmsUtils.nbtTagCompoundGet(nbtTag, "AttributeModifiers");
		}
		if (modifiersNbtTagList == null) {
			modifiersNbtTagList = NmsUtils.newNbtTagList();
		}
		//System.out.println(nmsStack.getTag());
		for (Attribute attribute : Attribute.values()) {
			AttributeInfo info = getAttributeInfo(attribute);
			if (info == null) {
				continue;
			}
			addAttribute(info);
			applyModifiers();
		}
		this.itemStack = stack;
	}

	/**
	 * After adding attributes, you need to call applyModifers() to actually apply them
	 *
	 * @param attribute attribute, @see Attribute
	 * @param operation what operatoin @see opration
	 * @param slot      what slot should be affected
	 * @param amount    what amount should be added
	 * @param name      what name
	 * @param uuid      what uuid
	 */
	public void addAttribute(Attribute attribute, Operation operation, Slot slot, double amount, String name, UUID uuid) {
		removeAttribute(attribute);
		Object compound = NmsUtils.newNbtTagCompound();
		NmsUtils.nbtTagCompoundSetString(compound, "AttributeName", attribute.getName());
		NmsUtils.nbtTagCompoundSetString(compound, "Name", name);
		NmsUtils.nbtTagCompoundSetString(compound, "Slot", slot.getName());
		NmsUtils.nbtTagCompoundSetInt(compound, "Operation", operation.i);
		NmsUtils.nbtTagCompoundSetDouble(compound, "Amount", amount);
		NmsUtils.nbtTagCompoundSetLong(compound, "UUIDMost", uuid.getMostSignificantBits());
		NmsUtils.nbtTagCompoundSetLong(compound, "UUIDLeast", uuid.getLeastSignificantBits());
		NmsUtils.nbtTagListAdd(modifiersNbtTagList, compound);
	}

	public void addAttribute(AttributeInfo info) {
		removeAttribute(info.attribute);
		Object compound = NmsUtils.newNbtTagCompound();
		NmsUtils.nbtTagCompoundSetString(compound, "AttributeName", info.attribute.getName());
		NmsUtils.nbtTagCompoundSetString(compound, "Name", info.name);
		NmsUtils.nbtTagCompoundSetString(compound, "Slot", info.slot.getName());
		NmsUtils.nbtTagCompoundSetInt(compound, "Operation", info.operation.i);
		NmsUtils.nbtTagCompoundSetDouble(compound, "Amount", info.amount);
		NmsUtils.nbtTagCompoundSetLong(compound, "UUIDMost", info.uuid.getMostSignificantBits());
		NmsUtils.nbtTagCompoundSetLong(compound, "UUIDLeast", info.uuid.getLeastSignificantBits());
		NmsUtils.nbtTagListAdd(modifiersNbtTagList, compound);
	}

	public void removeAttribute(Attribute attribute) {
		int size = NmsUtils.nbtTagListSize(modifiersNbtTagList);
		for (int i = 0; i < size; i++) {
			Object nbtBase = NmsUtils.nbtTaglistGet(modifiersNbtTagList, i);
			if (!NmsUtils.getNbtTagCompound().isInstance(nbtBase)) {
				continue;
			}
			if (NmsUtils.nbtTagCompoundGetString(nbtBase, "AttributeName").equals(attribute.getName())) {
				NmsUtils.nbtTagListRemove(modifiersNbtTagList, i);
			}
		}
	}

	public AttributeInfo getAttributeInfo(Attribute attribute) {
		AttributeInfo info = null;
		if (modifiersNbtTagList == null) {
			return null;
		}
		int size = NmsUtils.nbtTagListSize(modifiersNbtTagList);
		for (int i = 0; i < size; i++) {
			Object nbtBase = NmsUtils.nbtTaglistGet(modifiersNbtTagList, i);
			if (!NmsUtils.getNbtTagCompound().isInstance(nbtBase)) {
				continue;
			}
			if (NmsUtils.nbtTagCompoundGetString(nbtBase, "AttributeName").equals(attribute.getName())) {
				info = new AttributeInfo(
						attribute,
						Operation.fromInt(NmsUtils.nbtTagCompoundGetInt(nbtBase, "Operation")),
						Slot.byString(NmsUtils.nbtTagCompoundGetString(nbtBase, "Slot")),
						NmsUtils.nbtTagCompoundGetDouble(nbtBase, "Amount"),
						NmsUtils.nbtTagCompoundGetString(nbtBase, "Name"),
						new UUID(NmsUtils.nbtTagCompoundGetLong(nbtBase, "UUIDMost"),
								NmsUtils.nbtTagCompoundGetLong(nbtBase, "UUIDLeast"))
				);
				break;
			}
		}
		return info;
	}

	public void applyModifiers() {
		Object nmsStack = CBUtils.asNMSCopy(itemStack);
		Object nbtTag = NmsUtils.nmsStackGetOrCreateTag(nmsStack);
		NmsUtils.nbtTagCompoundSet(nbtTag, "AttributeModifiers", modifiersNbtTagList);
		//nmsStack.a("AttributeModifiers", modifiersNbtTagList); //a(String, NBTBase) = getOrCreateTag().set(String, NBTBase)
		itemStack = CBUtils.asBukkitCopy(nmsStack);
	}

	public void setCanDestroy(Material... materials) {
		setMaterialList("CanDestroy", materials);
	}

	public void setCanPlaceOn(Material... materials) {
		setMaterialList("CanPlaceOn", materials);
	}

	private void setMaterialList(String key, Material... materials) {
		Object canDestroyNbtTagList = NmsUtils.newNbtTagList();
		for (Material material : materials) {
			NmsUtils.nbtTagListAdd(canDestroyNbtTagList, NmsUtils.toNbtTagString(
					NmsUtils.getMinecraftKeyForBlock(
							NmsUtils.blockAsBlock(
									NmsUtils.nmsStackGetItem(
											CBUtils.asNMSCopy(new ItemStack(material))
									)
							)
					).toString()
			));
			//canDestroyNbtTagList.add(new NBTTagString(Block.REGISTRY.b(Block.asBlock(CraftItemStack.asNMSCopy(new ItemStack(material)).getItem())).toString()));
		}
		Object nmsStack = CBUtils.asNMSCopy(itemStack);
		Object nbtTag = NmsUtils.nmsStackGetOrCreateTag(nmsStack);
		NmsUtils.nbtTagCompoundSet(nbtTag, key, canDestroyNbtTagList);
		//nmsStack.a("CanDestroy", canDestroyNbtTagList);
		itemStack = CBUtils.asBukkitCopy(nmsStack);
	}

	public List<Material> getCanDestroy() {
		return getMaterialList("CanDestroy");
	}

	public List<Material> getCanPlaceOn() {
		return getMaterialList("CanPlaceOn");
	}

	private List<Material> getMaterialList(String key) {
		Object nmsStack = CBUtils.asNMSCopy(itemStack);
		List<Material> materials = new ArrayList<>();
		try {
			Object nbtTag = NmsUtils.nmsStackGetTag(nmsStack);
			Object tagList = NmsUtils.nbtTagCompoundGet(nbtTag, key);
			for (int a = 0; a < NmsUtils.nbtTagListSize(tagList); a++) {
				materials.add(
						CBUtils.asNewCraftStack(
								NmsUtils.getItemOf(
										NmsUtils.getBlockForMinecraftKey(
												NmsUtils.newMinecraftKey(
														NmsUtils.nbtTaglistGetString(tagList, a)
												)
										)
								)
						).getType()
				);
				//materials.add(CraftItemStack.asNewCraftStack(Item.getItemOf(Block.REGISTRY.get(new MinecraftKey(tagList.getString(a))))).getType());
			}
		} catch (Exception e) {
			return materials;
		}
		return materials;
	}

	public ItemStack getItemStack() {
		return itemStack;
	}

}
