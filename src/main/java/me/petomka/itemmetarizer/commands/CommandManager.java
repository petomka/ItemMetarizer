package me.petomka.itemmetarizer.commands;

import me.petomka.itemmetarizer.config.ConfigManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Benedikt on 23.08.2017.
 */
public class CommandManager {

    private static ArrayList<Command> commands = new ArrayList<>();

    public static void registerCommand(Command cmd) {
        commands.add(cmd);
    }

    public static void onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        for(Command command : commands) {
            if(command.getName().equalsIgnoreCase(cmd.getName())) {
                if(command.isPlayerOnly() && !(sender instanceof Player)) {
                    sender.sendMessage(ConfigManager.getString("from-console"));
                    return;
                }
                if(sender.hasPermission(command.getPermission())) {
                    if(!command.execute(sender, label, args)) {
                        sender.sendMessage(ConfigManager.getString("usage-format").replace("<usage>", command.getUsage()));
                    }
                } else {
                    sender.sendMessage(ConfigManager.getString("no-permission"));
                }
            }
        }
    }

}
