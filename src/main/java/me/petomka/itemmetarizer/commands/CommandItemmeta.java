package me.petomka.itemmetarizer.commands;

import me.petomka.itemmetarizer.config.ConfigManager;
import me.petomka.itemmetarizer.gui.ChatConfigurator;
import me.petomka.itemmetarizer.gui.MainGUI;
import me.petomka.itemmetarizer.gui.SavedItemsGUI;
import me.petomka.itemmetarizer.util.StringUtils;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;

/**
 * Created by Benedikt on 23.08.2017.
 */
public class CommandItemmeta extends Command {

    public CommandItemmeta() {
        super("itemmeta", "itemmetarizer.itemmeta", "/itemmeta help");
        this.setPlayerOnly(true);
        subcommands.add(new HelpSubCommand());
        subcommands.add(new SavesMenuSubCommand());
        subcommands.add(new ChatSubCommand());
    }

    private ArrayList<SubCommand> subcommands = new ArrayList<>();

    public boolean execute(CommandSender sender, String label, String[] args) {
        ItemStack hand = ((Player) sender).getInventory().getItemInMainHand();
        if (args.length == 0) {
            if (hand == null || hand.getType() == Material.AIR) {
                sender.sendMessage(ConfigManager.getString("command.no-item"));
                return false;
            } else {
                new MainGUI((Player) sender, hand);
            }
        } else {
            String[] subArgs = new String[args.length - 1];
            System.arraycopy(args, 1, subArgs, 0, args.length - 1);
            executeSubCommand(sender, args[0], subArgs);
        }
        return true;
    }

    private void executeSubCommand(CommandSender sender, String label, String[] args) {
        for (Command subcommand : subcommands) {
            if (subcommand.getName().equalsIgnoreCase(label)) {
                if (!StringUtils.isNullOrEmpty(subcommand.getPermission()) && sender.hasPermission(subcommand.getPermission())) {
                    if (!subcommand.execute(sender, label, args)) {
                        sender.sendMessage(ConfigManager.getString("usage-format").replace("<usage>", subcommand.getUsage()));
                    }
                } else {
                    sender.sendMessage(ConfigManager.getString("no-permission"));
                }
                return;
            }
        }
    }

    private abstract class SubCommand extends Command {

        private String description;

        public SubCommand(@Nonnull String name, @Nullable String permission, @Nonnull String usage, @Nonnull String description) {
            super(name, permission, usage);
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }

    private class HelpSubCommand extends SubCommand {

        public HelpSubCommand() {
            super("help", "itemmetarizer.itemmeta.help", "itemmeta help", "Shows this help page.");
        }

        @Override
        boolean execute(CommandSender sender, String label, String[] args) {
            int page = 0;
            if (args.length > 0) {
                try {
                    page = Integer.parseInt(args[0]) - 1;
                } catch (Exception e) {
                    page = 0;
                }
            }
            if(page < 0) {
                page = 0;
            }
            sender.sendMessage(ConfigManager.getString("command.help.header"));
            for (int i = 0; i < 8 && i + page * 8 < subcommands.size(); i++) {
                sender.sendMessage(ConfigManager.getString("command.help.format")
                        .replace("<command>", subcommands.get(i + page * 8).getUsage())
                        .replace("<description>", subcommands.get(i + page * 8).getDescription()));
            }
            sender.sendMessage(ConfigManager.getString("command.help.footer")
                    .replace("<page>", String.valueOf(page + 1))
                    .replace("<maxpage>", String.valueOf((subcommands.size() - 1) / 8 + 1)));
            return true;
        }
    }

    private class SavesMenuSubCommand extends SubCommand {

        public SavesMenuSubCommand() {
            super("saves", "itemmetarizer.itemmeta.saves", "itemmeta saves", "Opens the saved items");
        }

        @Override
        boolean execute(CommandSender sender, String label, String[] args) {
            new SavedItemsGUI((Player)sender, null);
            return true;
        }
    }

    private class ChatSubCommand extends SubCommand {


		public ChatSubCommand() {
			super("chat", null, "itemmeta chat", "Answer to a conversation");
		}

		@Override
		boolean execute(CommandSender sender, String label, String[] args) {
			if(!(sender instanceof Player)) {
				sender.sendMessage(ConfigManager.getString("from-console"));
				return true;
			}
			if (!ChatConfigurator.answerChatConfigurator((Player) sender, String.join(" ", args))) {
				sender.sendMessage(ConfigManager.getString("chat-config.not-in-config"));
			}
			return true;
		}
	}

}
