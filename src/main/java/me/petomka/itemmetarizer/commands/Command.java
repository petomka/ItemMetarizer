package me.petomka.itemmetarizer.commands;

import org.bukkit.command.CommandSender;

/**
 * Created by Benedikt on 23.08.2017.
 */
public abstract class Command {

    private final String name;
    private final String permission;
    private final String usage;
    private boolean isPlayerOnly = false;

    public Command(String name, String permission, String usage) {
        this.name = name;
        this.permission = permission;
        this.usage = usage;
        CommandManager.registerCommand(this);
    }

    public boolean isPlayerOnly() {
        return isPlayerOnly;
    }

    public void setPlayerOnly(boolean playerOnly) {
        isPlayerOnly = playerOnly;
    }

    public String getName() {
        return name;
    }

    public String getPermission() {
        return permission;
    }

    public String getUsage() {
        return usage;
    }

    abstract boolean execute(CommandSender sender, String label, String[] args);
}
