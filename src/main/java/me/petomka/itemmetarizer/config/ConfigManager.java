package me.petomka.itemmetarizer.config;

import me.petomka.itemmetarizer.Main;

/**
 * Created by Benedikt on 23.08.2017.
 */
public class ConfigManager {

    public static String getString(String path) {
        return Main.config.getString(path).replaceAll("&", "§").replaceAll("§§", "&").replaceAll("<prefix>", getPrefix());
    }

    private static String getPrefix() {
        return Main.config.getString("prefix").replaceAll("&", "§").replaceAll("§§", "&");
    }

}
